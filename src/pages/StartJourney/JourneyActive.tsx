import React, { useState, useEffect } from 'react';
import {
  IonContent,
  IonList,
  IonLabel,
  IonItem,
  IonAlert,
  IonToast,
  IonPopover,
  IonDatetime,
  useIonViewDidEnter,
  IonInput,
  IonSpinner,
  IonIcon,
} from '@ionic/react';
import { LocalNotifications } from '@ionic-native/local-notifications';

import './JourneyInfo.css';
import { useHistory } from 'react-router';

import {
  WLS_COMPLETE_JOURNEY,
  WLS_CANCEL_JOURNEY,
  WLS_CONFIG,
  WLS_UPDATE_JOURNEY,
  WLS_PROXY,
} from '../../services/Constants';
import { Journey } from '../../services/JourneyContext';
import { PrimaryButton, DangerButton } from '../../components/Buttons';
import Axios from 'axios';

import { InAppBrowser } from '@ionic-native/in-app-browser';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { closeCircleOutline } from 'ionicons/icons';

interface Props {
  journey: Journey;
  stopJourney: () => void;
  initaiteSOS: () => void;
  setJourney: (data: Journey) => void;
}

export default function JourneyActive({
  journey,
  stopJourney,
  initaiteSOS,
  setJourney,
}: Props) {
  const history = useHistory();
  const [terminate, setTerminate] = useState(false);
  const [toastMessage, setToastMessage] = useState('');
  const [popover, setPopover] = useState(false);
  const [arrivalDate, setArrivalDate] = useState<any>();
  const [reason, setReason] = useState('');
  const [alert, setAlert] = useState('');
  const [routeConfig, setRouteConfig] = useState<any>();
  const [loading, setLoading] = useState(false);

  useIonViewDidEnter(() => {
    setArrivalDate(new Date(journey.arrivalDate));
  });
  useEffect(() => {
    Axios.get(WLS_CONFIG).then((configRes) => {
      setRouteConfig(configRes.data.data.journeyConfig.RouteServiceConfig);
    });
  }, []);

  return (
    <IonContent>
      <IonList style={{ padding: '20px' }}>
        <IonItem>
          <IonLabel style={{ fontSize: '1.5rem' }}>
            {journey.journeyName}
          </IonLabel>
        </IonItem>
        <IonItem>
          <IonLabel style={{ color: '#c5c5c5' }}>Departure</IonLabel>
          <IonLabel style={{ marginLeft: '-40%' }}>
            {new Date(journey.departureDate).toLocaleString('en-US', {
              day: '2-digit',
              month: 'long',
              hour: '2-digit',
              minute: '2-digit',
            })}
          </IonLabel>
        </IonItem>
        <IonItem>
          <IonLabel style={{ color: '#c5c5c5' }}>Arrival</IonLabel>
          <IonLabel style={{ marginLeft: '-40%' }}>
            {new Date(journey.arrivalDate).toLocaleString('en-US', {
              day: '2-digit',
              month: 'long',
              hour: '2-digit',
              minute: '2-digit',
            })}
          </IonLabel>
        </IonItem>
        <div style={{ textAlign: 'center', marginTop: '10px' }}>
          {/* <FontAwesomeIcon icon={faSyncAlt} color="#0aa5e1" size="2x" /> */}
          <div className="loader" />
          <h3 style={{ marginTop: '40px' }}>This Journey is active</h3>
          <p style={{ padding: '0 20%', color: '#c5c5c5' }}>
            Remember to check in when you arrive at your destination
          </p>

          <div>
            <PrimaryButton
              size="large"
              onClick={() => {
                setPopover(true);
              }}
            >
              Update Arrival Time
            </PrimaryButton>
          </div>
          <div>
            <PrimaryButton
              onClick={async () => {
                try {
                  const user = JSON.parse(localStorage.getItem('userData')!);
                  await Axios.post(
                    `${WLS_COMPLETE_JOURNEY}?journeyId=${journey.journeyId}`,
                    {},
                    {
                      auth: {
                        username: user.networkId,
                        password: user.password,
                      },
                    },
                  );
                  stopJourney();
                  localStorage.removeItem('active_journey');

                  LocalNotifications.cancel(journey.journeyId);
                  setAlert('Your journey is completed!');
                  history.push('/home/tab1');
                } catch (error) {
                  console.error(error);
                }
              }}
              size="large"
            >
              Arrived
            </PrimaryButton>
          </div>
          <div>
            <DangerButton
              onClick={() => {
                initaiteSOS();
              }}
              size="large"
            >
              SOS - Get 911 Help
            </DangerButton>
          </div>
          <div>
            <PrimaryButton
              size="large"
              onClick={() => {
                setTerminate(true);
              }}
            >
              Terminate Journey
            </PrimaryButton>
          </div>
          <div>
            <PrimaryButton
              size="large"
              onClick={async () => {
                const configRes = await Axios.get(WLS_CONFIG);
                const {
                  JourneyServiceConfig,
                  GeometryServiceConfig,
                } = configRes.data.data.journeyConfig;
                const res = await Axios.get(
                  `${WLS_PROXY}?${JourneyServiceConfig.url}/query?where=JOURNEY_ID%3D%27${journey.journeyId}%27&returnGeometry=true&f=json`,
                );
                // const { x, y } = res.data.geometries[0];
                const data = res.data;
                const start = data.features[0].geometry;
                const end = data.features[1].geometry;
                const data2 = {
                  geometryType: 'esriGeometryPoint',
                  geometries: [start, end],
                };
                const converted = await Axios.get(
                  `${WLS_PROXY}?${GeometryServiceConfig.url}/project?inSR=${
                    GeometryServiceConfig.inSR
                  }&outSR=${GeometryServiceConfig.outSR}&geometries=${encodeURI(
                    JSON.stringify(data2),
                  )}&f=json`,
                );
                const start2 = converted.data.geometries[0];
                const end2 = converted.data.geometries[1];
                InAppBrowser.create(
                  `https://navigator.arcgis.app?stop=${end2.x},${end2.y}&start=${start2.x},${start2.y}`,
                  '_system',
                );
              }}
            >
              Open in SA Navigator
            </PrimaryButton>
          </div>
        </div>
      </IonList>
      <IonAlert
        isOpen={terminate}
        onDidDismiss={() => setTerminate(false)}
        subHeader="Please enter your reason"
        inputs={[
          {
            name: 'reason',
            type: 'text',
            placeholder: 'Please enter your reason',
          },
        ]}
        buttons={[
          {
            text: 'Back',
            role: 'cancel',
          },
          {
            text: 'Proceed',
            cssClass: 'secondary',
            handler: async (data) => {
              if (!data.reason) {
                setToastMessage('You have to provide a reason');
              } else {
                try {
                  await Axios.post(WLS_CANCEL_JOURNEY, {
                    journeyId: journey.journeyId,
                    remark: data.reason,
                  });
                  stopJourney();
                  localStorage.removeItem('active_journey');
                  history.push('/home/tab1');
                } catch (error) {
                  setToastMessage('Something went wrong');
                }
              }
            },
          },
        ]}
      />
      <IonPopover
        cssClass="safetravel-popover"
        isOpen={popover}
        onDidDismiss={() => setPopover(false)}
      >
        <IonIcon
          icon={closeCircleOutline}
          style={{
            position: 'absolute',
            right: '10px',
            top: '10px',
            fontSize: '2rem',
          }}
          onClick={() => setPopover(false)}
        />
        <h6 style={{ padding: '10px' }}>Enter new Arrival</h6>
        <IonItem style={{ margin: '10px' }}>
          <IonLabel style={{ maxWidth: '100px' }}>Arrival:</IonLabel>
          <IonDatetime
            style={{ padding: '0' }}
            onIonChange={(event) => {
              const date = new Date(event.detail.value!);
              setArrivalDate(date);
            }}
            // minuteValues="0,15,30,45"
            displayFormat="DD MMM h:mm a"
            value={arrivalDate ? arrivalDate.toUTCString() : ''}
            placeholder="Select time"
          />
        </IonItem>
        <IonItem style={{ margin: '10px' }}>
          <IonLabel style={{ width: '100px' }}>Reason:</IonLabel>
          <IonInput
            style={{ '--padding-start': 0 }}
            placeholder="Enter reason"
            onIonChange={(event) => {
              setReason(event.detail.value!);
            }}
          />
        </IonItem>
        <PrimaryButton
          disabled={!journey.distanceInKm || loading}
          style={{ margin: '20px auto', display: 'flex', width: '90%' }}
          onClick={async () => {
            setLoading(true);
            try {
              const currentLocation = await BackgroundGeolocation.getCurrentLocation();
              // const currentLocation = {
              // 	latitude: 26.37354,
              // 	longitude: 50.14196,
              // };
              const originGeometry = {
                x: currentLocation.longitude,
                y: currentLocation.latitude,
                spatialReference: {
                  wkid: 4326,
                },
              };
              const user = JSON.parse(localStorage.getItem('userData')!);
              const configRes = await Axios.get(WLS_CONFIG, {
                auth: {
                  username: user.networkId,
                  password: user.password,
                },
              });
              const {
                JourneyServiceConfig,
              } = configRes.data.data.journeyConfig;
              const destination = await Axios.get(
                `${WLS_PROXY}?${JourneyServiceConfig.url}/query?where=JOURNEY_ID%3D%27${journey.journeyId}%27&returnGeometry=true&f=json`,
              );
              const destinationData = destination.data;
              const end = {
                name: destinationData.features[1].attributes.NAME,
                geometry: {
                  x: destinationData.features[1].geometry.x,
                  y: destinationData.features[1].geometry.y,
                  spatialReference: {
                    wkid: 3857,
                  },
                },
              };
              const stops = {
                type: 'features',
                features: [
                  {
                    geometry: originGeometry,
                    attributes: {
                      CurbApproach: null,
                      TimeWindowStart: null,
                      TimeWindowEnd: null,
                      Name: 'Current Location',
                    },
                  },
                  {
                    geometry: end.geometry,
                    attributes: {
                      CurbApproach: null,
                      TimeWindowStart: null,
                      TimeWindowEnd: null,
                      Name: end.name,
                    },
                  },
                ],
                doNotLocateOnRestrictedElements: true,
              };

              const res = await Axios.get(
                `${WLS_PROXY}?${
                  routeConfig.url
                }/solve?&f=json&stops=${encodeURI(JSON.stringify(stops))}`,
              );
              const data = res.data.directions[0].summary;
              console.log(data);
              setArrivalDate(
                new Date(
                  new Date().getTime() + data.totalDriveTime * 60 * 1000,
                ),
              );
            } catch (ex) {
              console.log(ex);
              setToastMessage('Something went wrong');
            }
            setLoading(false);
          }}
        >
          {loading && <IonSpinner />}
          {!loading && 'Recalculate From Now'}
        </PrimaryButton>
        <PrimaryButton
          disabled={!arrivalDate}
          style={{ margin: '20px auto', display: 'flex', width: '90%' }}
          onClick={async () => {
            try {
              const updated = arrivalDate!.getTime();
              if (updated < new Date().getTime()) {
                setToastMessage('You cannot select a date in the past');
                return;
              }
              const data = {
                journeyId: journey.journeyId,
                arrivalDate: updated,
                remark: reason,
              };
              await Axios.post(WLS_UPDATE_JOURNEY, data);
              setPopover(false);
              setToastMessage('Journey Arrival Time Updated');
              setJourney({
                ...journey,
                arrivalDate: updated,
              });
            } catch (ex) {
              setToastMessage('Something went wrong!');
            }
          }}
        >
          Submit
        </PrimaryButton>
      </IonPopover>

      <IonToast
        isOpen={!!toastMessage}
        onDidDismiss={() => setToastMessage('')}
        duration={1000}
        message={toastMessage}
      />
      <IonAlert
        message={alert}
        onDidDismiss={() => setAlert('')}
        isOpen={!!alert}
        buttons={['OK']}
      />
    </IonContent>
  );
}
