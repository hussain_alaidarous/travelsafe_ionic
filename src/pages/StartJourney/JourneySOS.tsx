import React, { useState, useEffect } from 'react';
import { IonContent, IonList, IonLabel, IonItem, IonCard, IonCardContent, IonSpinner } from '@ionic/react';

import './JourneyInfo.css';
import { Journey } from '../../services/JourneyContext';
import { CallNumber } from '@ionic-native/call-number';

import { WLS_SOS_JOURNEY, WLS_USER_POSITION } from '../../services/Constants';
import { DangerButton, ClearButton } from '../../components/Buttons';
import Axios from 'axios';
import { useHistory } from 'react-router';

import { BackgroundGeolocation } from '@ionic-native/background-geolocation';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';

interface Props {
	journey: Journey;
	cancelSOS: () => void;
	stopJourney: () => void;
}

export default function JourneySOS({ journey, cancelSOS, stopJourney }: Props) {
	const history = useHistory();
	const [ status, setStatus ] = useState('');
	const [ counter, setCounter ] = useState(0);
	useEffect(
		() => {
			if (counter === 1) {
				localStorage.setItem('SOS_status', 'confirmed');
				setStatus('confirmed');
				BackgroundGeolocation.getCurrentLocation({
					enableHighAccuracy: true,
					maximumAge: 10000,
					timeout: 10000,
				}).then(async (location) => {
					const locationData = {
						journeyId: journey.journeyId,
						userPositions: [
							{
								positionDate: location.time.toFixed(0),
								geometry: {
									x: location.longitude,
									y: location.latitude,
									spatialReference: {
										wkid: 4326,
									},
								},
							},
						],
					};
					await Axios.post(WLS_USER_POSITION, locationData);
				});
				Axios.post(`${WLS_SOS_JOURNEY}?journeyId=${journey.journeyId}`);

				localStorage.setItem(
					'active_journey',
					JSON.stringify({
						...journey,
						journeyStatus: {
							id: 9,
							status: 'SOS',
						},
					}),
				);
				CallNumber.callNumber('911', true)
					.then((res) => console.log('Launched dialer!', res))
					.catch((err) => console.log('Error launching dialer', err));
			}
			const timer: any = counter > 0 && setInterval(() => setCounter(counter - 1), 1000);
			return () => clearInterval(timer);
		}, // eslint-disable-next-line
		[ counter, journey.journeyId ],
	);
	useEffect(
		() => {
			// localStorage.removeItem('SOS_status');
			const SOS_status = localStorage.getItem('SOS_status');
			setStatus(SOS_status!);
		},
		[ setStatus ],
	);
	return (
		<IonContent>
			<IonList style={{ padding: '20px' }}>
				<IonItem>
					<IonLabel style={{ fontSize: '1.5rem' }}>{journey.journeyName}</IonLabel>
				</IonItem>
				<IonItem>
					<IonLabel style={{ color: '#c5c5c5' }}>Departure</IonLabel>
					<IonLabel style={{ marginLeft: '-50%' }}>
						{new Date(journey.departureDate).toLocaleString('en-US', {
							day: '2-digit',
							month: 'long',
							hour: '2-digit',
							minute: '2-digit',
						})}
					</IonLabel>
				</IonItem>
				<IonItem>
					<IonLabel style={{ color: '#c5c5c5' }}>Arrival</IonLabel>
					<IonLabel style={{ marginLeft: '-50%' }}>
						{new Date(journey.arrivalDate).toLocaleString('en-US', {
							day: '2-digit',
							month: 'long',
							hour: '2-digit',
							minute: '2-digit',
						})}
					</IonLabel>
				</IonItem>
				<FontAwesomeIcon icon="exclamation-triangle" />
				<IonCard style={{ textAlign: 'center' }}>
					<IonCardContent>
						<div style={{ display: 'flex', marginTop: '10px' }}>
							<div style={{ margin: 'auto' }}>
								<div>
									<FontAwesomeIcon icon={faExclamationTriangle} color="#d22630" size="10x" />
									{/* <IonIcon
										style={{ height: '10rem', width: '10rem' }}
										size="large"
										color="danger"
										icon={triangle}
									/> */}
								</div>
							</div>
						</div>
						<h2>Emergency Help</h2>
						{status &&
						status === 'initiated' && (
							<React.Fragment>
								<p>Confirm your need for emergency help</p>
								{!counter && (
									<div>
										<DangerButton
											size="large"
											onClick={() => {
												// localStorage.setItem('SOS_status', 'confirmed');
												// setStatus('confirmed');
												setCounter(6);
												// setShowAlert(`Sending SOS signal in: ${counter}`);
											}}
										>
											Confirm SOS signal
										</DangerButton>
									</div>
								)}
								{!!counter && (
									<div>
										<DangerButton disabled size="large" onClick={() => {}}>
											<IonSpinner />
											{`Sending SOS in: ${counter - 1}`}
										</DangerButton>
									</div>
								)}
								<div>
									<ClearButton
										size="large"
										onClick={() => {
											setCounter(0);
											cancelSOS();
										}}
									>
										Cancel SOS
									</ClearButton>
								</div>
							</React.Fragment>
						)}
						{status &&
						status === 'confirmed' && (
							<React.Fragment>
								<h1 style={{ display: 'inline-flex' }}>
									{/* <IonIcon
										style={{ height: '4rem', width: '4rem' }}
										size="large"
										color="success"
										icon={checkmark}
									/> */}
									<b>
										<span style={{ margin: 'auto 0' }}>
											STAY WITH YOUR VEHICLE - DO NOT LEAVE IT!
										</span>
									</b>
								</h1>
							</React.Fragment>
						)}

						{status &&
						status === 'confirmed' && (
							<div style={{ textAlign: 'center', marginTop: '10px' }}>
								<DangerButton
									size="large"
									onClick={() => {
										CallNumber.callNumber('911', true)
											.then((res) => console.log('Launched dialer!', res))
											.catch((err) => console.log('Error launching dialer', err));
									}}
								>
									Call 911 Now
								</DangerButton>
							</div>
						)}
					</IonCardContent>
				</IonCard>

				{status &&
				status === 'confirmed' && (
					<div style={{ textAlign: 'center', marginTop: '10px' }}>
						<ClearButton
							size="large"
							onClick={() => {
								stopJourney();
								history.push('/home/tab1');
							}}
						>
							Close Journey
						</ClearButton>
					</div>
				)}
			</IonList>
		</IonContent>
	);
}
