import React, { useEffect, useState, useContext } from 'react';
import { useHistory, useParams } from 'react-router';
import {
	IonPage,
	IonContent,
	IonList,
	IonLabel,
	IonLoading,
	IonItem,
	IonCardTitle,
	IonAlert,
	IonToast,
	IonPopover,
	IonDatetime,
	IonInput,
} from '@ionic/react';
import { Journey, JourneyContext } from '../../services/JourneyContext';

import { WLS_CANCEL_JOURNEY, WLS_CONFIG, WLS_PROXY, WLS_USER_JOURNEY } from '../../services/Constants';

import './JourneyInfo.css';
import { Header } from '../../components/Header';
import { StatusIcon } from '../../components/StatusIcon';
import { ClearButton, PrimaryButton } from '../../components/Buttons';
import Axios from 'axios';

export default function JourneyInfo() {
	const history = useHistory();
	const context = useContext(JourneyContext);
	const { id } = useParams<{ id: string }>();
	const [ journey, setJourney ] = useState<Journey | undefined>();
	const [ cancel, setCancel ] = useState(false);
	const [ toastMessage, setToastMessage ] = useState('');
	const [ popover, setPopover ] = useState<'' | 'return' | 'repeat'>('');
	const [ departureDate, setDepartureDate ] = useState<Date>();
	const [ duration, setDuration ] = useState('');
	const [ alert, setAlert ] = useState('');
	const [ start, setStart ] = useState<any>();
	const [ end, setEnd ] = useState<any>();
	const [ loading, setloading ] = useState(false);
	const [ name, setName ] = useState('');

	const getJourneyLocations = async () => {
		setloading(true);
		const configRes = await Axios.get(WLS_CONFIG);
		const { JourneyServiceConfig } = configRes.data.data.journeyConfig;
		const res = await Axios.get(
			`${WLS_PROXY}?${JourneyServiceConfig.url}/query?where=JOURNEY_ID%3D%27${id}%27&returnGeometry=true&f=json`,
		);
		const data = res.data;
		setStart({
			name: data.features[0].attributes.NAME,
			geometry: {
				x: data.features[0].geometry.x,
				y: data.features[0].geometry.y,
				spatialReference: {
					wkid: 3857,
				},
			},
		});
		setEnd({
			name: data.features[1].attributes.NAME,
			geometry: {
				x: data.features[1].geometry.x,
				y: data.features[1].geometry.y,
				spatialReference: {
					wkid: 3857,
				},
			},
		});
		setloading(false);
	};
	useEffect(
		() => {
			getJourneyLocations();
			Axios.get(`${WLS_USER_JOURNEY}?journeyId=${parseInt(id!)}`).then((res) => {
				const selected: Journey = res.data.data;
				// const selected = list.filter((single) => single.journeyId === parseInt(id!))[0];
				const seconds = (selected.arrivalDate - selected.departureDate) / 1000;
				const days = Math.floor(seconds / (60 * 60 * 24));
				const hours = Math.floor((seconds - days * 60 * 60 * 24) / (60 * 60));
				const mins = Math.floor((seconds - days * 60 * 60 * 24 - hours * 60 * 60) / 60);
				let temp = '';
				if (days) temp += days + ' days ';
				if (hours) temp += hours + ' hours ';
				if (mins) temp += mins + ' mins ';
				setDuration(temp);
				setJourney(selected);
			});
		}, // eslint-disable-next-line
		[ id ],
	);
	if (!journey) return <IonLoading isOpen />;
	const { status } = journey.journeyStatus;
	return (
		<IonPage>
			<Header backButton />
			<IonContent style={{ padding: '20px' }}>
				<h6 style={{ marginLeft: '30px' }}>Journey Details</h6>
				<IonList style={{ padding: '20px' }}>
					<IonCardTitle style={{ marginLeft: '10px' }}>{journey.journeyName}</IonCardTitle>
					{/* <IonCardSubtitle style={{ marginLeft: '10px' }}>{journey.purpose}</IonCardSubtitle> */}
					<IonItem>
						<IonLabel style={{ display: 'contents' }}>
							<StatusIcon isCard={false} status={status} />
							{status}
						</IonLabel>
					</IonItem>
					<IonItem>
						<IonLabel style={{ color: '#c5c5c5' }}>Driver</IonLabel>
						<IonLabel style={{ marginLeft: '-40%' }}>{journey.driver.fullName}</IonLabel>
					</IonItem>
					<IonItem>
						<IonLabel style={{ color: '#c5c5c5' }}>From</IonLabel>
						<IonLabel style={{ marginLeft: '-40%' }}>{start ? start.name : ''}</IonLabel>
					</IonItem>
					<IonItem>
						<IonLabel style={{ color: '#c5c5c5' }}>To</IonLabel>
						<IonLabel style={{ marginLeft: '-40%' }}>{end ? end.name : ''}</IonLabel>
					</IonItem>
					<IonItem>
						<IonLabel style={{ color: '#c5c5c5' }}>Departure</IonLabel>
						<IonLabel style={{ marginLeft: '-40%' }}>
							{new Date(journey.departureDate).toLocaleString('en-US', {
								day: '2-digit',
								month: 'long',
								hour: '2-digit',
								minute: '2-digit',
							})}
						</IonLabel>
					</IonItem>
					<IonItem>
						<IonLabel style={{ color: '#c5c5c5' }}>Arrival</IonLabel>
						<IonLabel style={{ marginLeft: '-40%' }}>
							{new Date(journey.arrivalDate).toLocaleString('en-US', {
								day: '2-digit',
								month: 'long',
								hour: '2-digit',
								minute: '2-digit',
							})}
						</IonLabel>
					</IonItem>
					<IonItem>
						<IonLabel style={{ color: '#c5c5c5' }}>Distance</IonLabel>
						<IonLabel style={{ marginLeft: '-40%' }}>
							{journey.distanceInKm ? `${journey.distanceInKm} km` : 'Not Calculated'}
						</IonLabel>
					</IonItem>
					<IonItem>
						<IonLabel style={{ color: '#c5c5c5' }}>Duration</IonLabel>
						<IonLabel style={{ marginLeft: '-40%' }}>{duration}</IonLabel>
					</IonItem>
				</IonList>
				<IonToast
					isOpen={!!toastMessage}
					onDidDismiss={() => setToastMessage('')}
					duration={1000}
					message={toastMessage}
				/>

				<IonAlert
					isOpen={cancel}
					onDidDismiss={() => setCancel(false)}
					// header={'Are you sure?'}
					subHeader="Please enter your reason"
					inputs={[
						{
							name: 'reason',
							type: 'text',
							placeholder: 'Please enter your reason',
						},
					]}
					buttons={[
						{
							text: 'Back',
							role: 'cancel',
						},
						{
							text: 'Proceed',
							cssClass: 'secondary',
							handler: async (data) => {
								if (!data.reason) {
									setToastMessage('You have to provide a reason');
								} else {
									try {
										await Axios.post(WLS_CANCEL_JOURNEY, {
											journeyId: journey.journeyId,
											remark: data.reason,
										});
										setAlert('Journey has been Canceled');
										history.push('/home/tab1');
									} catch (error) {
										setAlert('Something went wrong');
									}
								}
							},
						},
					]}
				/>
				{status === 'Approved' &&
				journey.departureDate <= new Date().getTime() + 3 * 1000 * 60 * 60 && (
					<div style={{ display: 'flex', marginTop: '10px' }}>
						<PrimaryButton
							style={{
								margin: 'auto',
							}}
							size="large"
							onClick={() => {
								const isActive = localStorage.getItem('active_journey');
								if (isActive) {
									setAlert('You cannot start a journey while another one is active');
									return;
								}
								history.push(`/journey/start/checklist/${id}`);
							}}
						>
							Start this Journey
						</PrimaryButton>
					</div>
				)}
				{/* {status !== 'Completed' &&
				status !== 'Canceled' &&
				status !== 'SOS' && (
					<div style={{ display: 'flex', marginTop: '10px' }}>
						<DangerButton
							style={{
								margin: 'auto',
							}}
							size="large"
							onClick={() => {
								setCancel(true);
							}}
						>
							Cancel this Journey
						</DangerButton>
					</div>
				)} */}
				{/* {(status === 'Active' || status === 'Late') && (
					<div style={{ display: 'flex', marginTop: '10px' }}>
						<PrimaryButton
							style={{ margin: 'auto' }}
							size="large"
							onClick={() => {
								localStorage.setItem('active_journey', JSON.stringify(journey));
								history.replace('/home/tab2');
							}}
						>
							View this Journey
						</PrimaryButton>
					</div>
				)} */}

				<div style={{ display: 'flex', marginTop: '10px' }}>
					<PrimaryButton
						style={{ margin: 'auto' }}
						size="large"
						onClick={async () => {
							setDepartureDate(undefined);
							setPopover('return');
						}}
					>
						Create Return Journey
					</PrimaryButton>
				</div>

				<div style={{ display: 'flex', marginTop: '10px' }}>
					<PrimaryButton
						style={{ margin: 'auto' }}
						size="large"
						onClick={async () => {
							setDepartureDate(undefined);
							setPopover('repeat');
						}}
					>
						Repeat Journey
					</PrimaryButton>
				</div>

				{/* <div style={{ display: 'flex', marginTop: '10px' }}>
					<PrimaryButton
						style={{ margin: 'auto' }}
						size="large"
						onClick={() => {
							const favorite = localStorage.getItem('favorite_journeys');
							if (favorite) {
								const list: number[] = JSON.parse(favorite);
								if (isFavorite) {
									list.splice(list.indexOf(journey.journeyId), 1);
									setIsFavorite(false);
								} else {
									list.push(journey.journeyId);
									setIsFavorite(true);
								}
								localStorage.setItem('favorite_journeys', JSON.stringify(list));
							} else {
								const list = [ journey.journeyId ];
								localStorage.setItem('favorite_journeys', JSON.stringify(list));
							}
						}}
					>
						{isFavorite ? 'Remove From' : 'Add to'} Favorites
					</PrimaryButton>
				</div> */}
				<div style={{ display: 'flex', marginTop: '10px', marginBottom: '20px' }}>
					<ClearButton
						style={{ margin: 'auto' }}
						size="large"
						onClick={() => {
							history.goBack();
						}}
					>
						Back
					</ClearButton>
				</div>
				<IonPopover
					mode="ios"
					cssClass="safetravel-popover"
					isOpen={!!popover}
					onDidDismiss={() => setPopover('')}
				>
					<h6 style={{ padding: '10px' }}>Enter departure date for {popover} journey</h6>
					<IonItem style={{ margin: '10px' }}>
						<IonLabel style={{ maxWidth: '100px' }}>Departure:</IonLabel>
						<IonDatetime
							style={{ padding: '0' }}
							onIonChange={(event) => {
								const date = new Date(event.detail.value!);
								setDepartureDate(date);
							}}
							// minuteValues="0,15,30,45"
							placeholder="Select Departure Time"
							displayFormat="DD MMM h:mm a"
						/>
					</IonItem>
					<IonItem style={{ margin: '10px' }}>
						<IonLabel style={{ width: '100px' }}>Name:</IonLabel>
						<IonInput
							onIonChange={(event) => {
								setName(event.detail.value!);
							}}
							value={name}
							placeholder={`${popover.toUpperCase()}: ${journey.journeyName}`}
							// minuteValues="0,15,30,45"
						/>
					</IonItem>
					<PrimaryButton
						disabled={!departureDate}
						style={{ margin: '20px auto', display: 'flex', width: '90%' }}
						onClick={async () => {
							context.setJourney({
								...journey,
								journeyName: name || `${popover.toUpperCase()}: ${journey.journeyName}`,
								journeyLocations: {
									journeyStart: popover === 'repeat' ? start : end,
									journeyEnd: popover === 'repeat' ? end : start,
								},
								departureDate: departureDate!.getTime(),
								arrivalDate: departureDate!.getTime() + (journey.arrivalDate - journey.departureDate),
								passengers: journey.passengers || [],
							});
							setPopover('');
							history.push('/journey/new/summary');
						}}
					>
						Show Summary
					</PrimaryButton>
				</IonPopover>
				<IonLoading isOpen={loading} />
				<IonAlert message={alert} onDidDismiss={() => setAlert('')} isOpen={!!alert} buttons={[ 'OK' ]} />
			</IonContent>
		</IonPage>
	);
}
