import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router';
import {
	IonPage,
	IonContent,
	IonIcon,
	IonButton,
	IonList,
	IonLabel,
	IonLoading,
	IonItem,
	IonCardTitle,
	IonCheckbox,
	IonText,
	IonToast,
	IonSearchbar,
	IonModal,
	IonSpinner,
	IonRadioGroup,
	IonRadio,
	IonInput,
} from '@ionic/react';
import { checkmarkCircleOutline } from 'ionicons/icons';
import { Journey } from '../../services/JourneyContext';

import {
	WLS_START_JOURNEY,
	WLS_CONFIG,
	WLS_USER_POSITION,
	WLS_PROXY,
	WLS_USER_JOURNEY,
} from '../../services/Constants';

import './JourneyInfo.css';
import { Header } from '../../components/Header';
import Axios from 'axios';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';

export default function JourneyChecklist() {
	const { id } = useParams<{ id: string }>();
	const history = useHistory();
	const [ journey, setJourney ] = useState<Journey | undefined>();
	const [ agreed, setAgreed ] = useState(false);
	const [ error, setError ] = useState('');
	const [ vehicleId, setVehicleId ] = useState('');
	const [ checkList, setCheckList ] = useState('');
	const [ vehicleList, setVehicleList ] = useState([] as string[]);
	const [ filteredList, setFilteredList ] = useState([] as string[]);
	const [ show, setShow ] = useState(false);
	const [ saVehicle, setSaVehicle ] = useState(true);
	useEffect(
		() => {
			Axios.get(`${WLS_USER_JOURNEY}?journeyId=${parseInt(id!)}`).then((res) => {
				const selected: Journey = res.data.data;
				// const selected = list.filter((single) => single.journeyId === parseInt(id!))[0];
				setJourney(selected);
			});
		},
		[ id ],
	);
	useEffect(() => {
		Axios.get(WLS_CONFIG).then((res) => {
			const empConfig = res.data.data.journeyConfig;
			setCheckList(empConfig.journeyChecklist);
			const config = empConfig.AVLDoorServiceConfig;
			Axios.get(
				`${WLS_PROXY}?${config.url}/query?f=json&where=1=1&outFields=${config.field}`,
			).then((vehicles) => {
				console.log(vehicles.data.features[0]);
				setVehicleList(vehicles.data.features.map((rec) => rec.attributes.DOOR_NO));
			});
		});
	}, []);
	if (!journey) return <IonLoading isOpen />;
	return (
		<IonPage>
			<Header backButton />
			<IonContent style={{ padding: '20px' }}>
				<IonList style={{ padding: '20px' }}>
					<IonCardTitle style={{ marginLeft: '10px' }}>{journey.journeyName}</IonCardTitle>
					{/* <IonCardSubtitle style={{ marginLeft: '10px' }}>{journey.purpose}</IonCardSubtitle> */}
					<IonItem>
						<IonLabel style={{ display: 'contents' }}>
							<IonIcon
								icon={checkmarkCircleOutline}
								style={{
									margin: '0 10px 0 0',
									color: 'white',
									background: '#0aa5e1',
									fontSize: '1.5rem',
									borderRadius: '50%',
								}}
							/>
							{journey.journeyStatus.status}
						</IonLabel>
					</IonItem>
					<IonItem>
						<IonLabel style={{ color: '#c5c5c5' }}>Departure</IonLabel>
						<IonLabel style={{ marginLeft: '-40%' }}>
							{new Date(journey.departureDate).toLocaleString('en-US', {
								day: '2-digit',
								month: 'long',
								hour: '2-digit',
								minute: '2-digit',
							})}
						</IonLabel>
					</IonItem>
					<IonItem>
						<IonLabel style={{ color: '#c5c5c5' }}>Arrival</IonLabel>
						<IonLabel style={{ marginLeft: '-40%' }}>
							{new Date(journey.arrivalDate).toLocaleString('en-US', {
								day: '2-digit',
								month: 'long',
								hour: '2-digit',
								minute: '2-digit',
							})}
						</IonLabel>
					</IonItem>
					{/* <IonItem>
						<IonCheckbox />
						<IonLabel style={{ color: '#c5c5c5' }}>Using a Private Vehicle</IonLabel>
					</IonItem> */}

					{/* <IonList> */}
					<IonItem style={{ marginTop: '10px' }}>
						<IonLabel>Vehicle Type</IonLabel>
					</IonItem>
					<IonRadioGroup
						value={saVehicle}
						onIonChange={(event) => {
							setSaVehicle(event.detail.value);
							setVehicleId('');
						}}
					>
						<IonItem>
							<IonRadio mode="md" value={true} />
							<IonLabel style={{ marginLeft: '10px' }}>SA Vehicle</IonLabel>
						</IonItem>
						<IonItem>
							<IonRadio mode="md" value={false} />
							<IonLabel style={{ marginLeft: '10px' }}>Non-SA Vehicle</IonLabel>
						</IonItem>
					</IonRadioGroup>
					<IonItem style={{ marginTop: '10px' }}>
						<IonLabel>Vehicle Door/ID Number*</IonLabel>
					</IonItem>
					{/* </IonList> */}
					{saVehicle && (
						<IonItem style={{ marginBottom: '10px', border: vehicleId ? '' : '1px solid red' }}>
							<IonLabel
								onClick={() => setShow(true)}
								style={{ width: '100%', color: vehicleId ? 'black' : 'grey' }}
								// value=
							>
								{vehicleId || 'Click To Select Vehicle'}
							</IonLabel>
						</IonItem>
					)}
					{!saVehicle && (
						<IonItem style={{ marginBottom: '10px', border: vehicleId ? '' : '1px solid red' }}>
							<IonInput
								placeholder="Enter Vehicle Number"
								value={vehicleId}
								onIonChange={(event) => setVehicleId(event.detail.value || '')}
							/>
						</IonItem>
					)}
					<div dangerouslySetInnerHTML={{ __html: checkList }} style={{ paddingLeft: '15px' }} />
					<IonItem>
						<IonText>
							I hereby declare that I have followed all the procedures outlined in the Company’s Journey
							Management Program relating to the traveler(s) and this journey detailed within.
						</IonText>
						<IonCheckbox
							checked={agreed}
							onIonChange={(event) => setAgreed(event.detail.checked)}
							slot="start"
						/>
					</IonItem>
				</IonList>
				<IonModal isOpen={show}>
					<IonSearchbar
						style={{ marginTop: '50px', marginBottom: '20px' }}
						mode="md"
						placeholder="Search For Vehicle Door No."
						// debounce={700}
						onIonChange={async (event) => {
							const search = event.detail.value || '';
							setFilteredList(vehicleList.filter((rec) => rec.includes(search.toUpperCase())));
						}}
					/>
					<IonList style={{ minHeight: '100%' }}>
						{!vehicleList.length ? (
							<IonSpinner style={{ margin: 'auto', display: 'flex' }} />
						) : !filteredList.length ? (
							<IonItem>
								<IonButton style={{ marginTop: '15px' }} color="medium" onClick={() => setShow(false)}>
									Back
								</IonButton>
							</IonItem>
						) : (
							filteredList.filter((_, index) => index < 20).map((rec) => (
								<IonItem
									onClick={() => {
										setVehicleId(rec);
										setShow(false);
										setFilteredList([]);
									}}
									key={rec}
								>
									{rec}
								</IonItem>
							))
						)}
					</IonList>
				</IonModal>
				<IonToast isOpen={!!error} onDidDismiss={() => setError('')} duration={1000} message={error} />
				{journey &&
				journey.journeyStatus.id === 1 && (
					<div style={{ display: 'flex', marginTop: '10px', marginBottom: '20px' }}>
						<IonButton
							className="cancel-button"
							color="primary"
							style={{
								margin: 'auto',
								width: '54%',
								color: 'grey',
								height: '50px',
							}}
							disabled={!agreed || !vehicleId}
							onClick={async () => {
								try {
									localStorage.removeItem('SOS_status');
									localStorage.setItem('active_journey', JSON.stringify(journey));
									try {
										const data = await BackgroundGeolocation.getCurrentLocation();

										const locationData = {
											journeyId: journey.journeyId,
											userPositions: [
												{
													positionDate: data.time.toFixed(0),
													geometry: {
														x: data.longitude,
														y: data.latitude,
														spatialReference: {
															wkid: 4326,
														},
													},
												},
											],
										};
										await Axios.post(WLS_USER_POSITION, locationData);
									} catch (ex) {}
									const res = await Axios.post(`${WLS_START_JOURNEY}`, {
										journeyId: journey.journeyId,
										vehicleId,
									});
									if (!res.data.success) {
										throw Error('Something Went Wrong');
									}
									history.replace('/home/tab2');
								} catch (error) {
									setError('Sorry, something went wrong');
								}
							}}
						>
							Start this Journey
						</IonButton>
					</div>
					// <div style={{ display: 'flex', marginTop: '10px' }}>
					// 	<IonButton
					// 		// routerLink={`/journey/star/details`}
					// 		disabled={!agreed || !vehicleId}
					// 		color="success"
					// 		style={{ margin: 'auto', width: '54%' }}
					// 		onClick={async () => {
					// 			try {
					// 				await Axios.post(`${WLS_START_JOURNEY}`, {
					// 					journeyId: journey.journeyId,
					// 					vehicleId,
					// 				});
					// 				localStorage.removeItem('SOS_status');
					// 				localStorage.setItem('active_journey', JSON.stringify(journey));
					// 				history.replace('/home/tab2');
					// 			} catch (error) {
					// 				setError('Sorry, something went wrong');
					// 			}
					// 		}}
					// 	>
					// 		<IonIcon icon={checkmark} />Start this Journey
					// 	</IonButton>
					// </div>
				)}
			</IonContent>
		</IonPage>
	);
}
