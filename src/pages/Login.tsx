import React, { useState } from 'react';
import {
  IonPage,
  IonContent,
  IonCard,
  IonCardTitle,
  IonCardHeader,
  IonCardContent,
  IonItem,
  IonInput,
  IonImg,
  IonIcon,
  IonModal,
  IonAlert,
  IonButton,
} from '@ionic/react';
import { WLS_USER_INFO } from '../services/Constants';
import { PrimaryButton } from '../components/Buttons';
import safeTravel from '../theme/icon_colored.png';
import { person, key, closeCircleOutline } from 'ionicons/icons';
import { TestingPage } from '../components/TestingPage';
import Axios from 'axios';
import { Document, Page } from 'react-pdf/dist/esm/entry.webpack';

export default function Login() {
  const [networkId, setNetworkId] = useState('');
  const [password, setPassword] = useState('');
  const [showToast, setShowToast] = useState('');
  const [loading, setLoading] = useState(false);
  const [testing, setTesting] = useState(false);
  const [numPages, setNumPages] = useState(null);
  const [terms, setTerms] = useState('');

  function onDocumentLoadSuccess({ numPages }) {
    setNumPages(numPages);
  }

  return (
    <IonPage>
      {/* <Header /> */}
      <IonContent>
        <IonImg src={safeTravel} style={{ height: '200px' }} />
        <IonCard style={{ boxShadow: 'none' }}>
          <IonCardHeader style={{ textAlign: 'center' }}>
            <IonCardTitle style={{ margin: 'auto' }}>
              Login to SafeTravel App
            </IonCardTitle>
          </IonCardHeader>
          <IonCardContent>
            <IonItem style={{ border: '1px solid black', borderRadius: '5px' }}>
              <IonIcon icon={person} style={{ marginRight: '20px' }} />
              <IonInput
                placeholder="Network ID"
                onIonChange={(event) => setNetworkId(event.detail.value!)}
              />
            </IonItem>
            <IonItem
              style={{
                border: '1px solid black',
                borderRadius: '5px',
                marginTop: '10px',
              }}
            >
              <IonIcon icon={key} style={{ marginRight: '20px' }} />
              <IonInput
                placeholder="Password"
                type="password"
                value={password}
                onIonChange={(event) => setPassword(event.detail.value!)}
              />
            </IonItem>
            <PrimaryButton
              style={{ display: 'flex', margin: 'auto', marginTop: '20px' }}
              disabled={loading || !networkId || !password}
              size="large"
              color="travel"
              onClick={async () => {
                try {
                  setLoading(true);
                  const res = await Axios.get(
                    `${WLS_USER_INFO}?networkId=${networkId}`,
                    {
                      auth: {
                        username: networkId,
                        password,
                      },
                      withCredentials: false,
                    },
                  );
                  const data = res.data;
                  if (!data.success) {
                    setLoading(false);
                    throw Error('User not found');
                  }
                  setShowToast('');
                  console.log({ data });
                  localStorage.setItem(
                    'userData',
                    JSON.stringify({ networkId, password }),
                  );
                  window.location.reload();
                } catch (error) {
                  setLoading(false);
                  console.error(error);
                  setShowToast('Login Error, Please check your credentials');
                  setPassword('');
                }
              }}
            >
              Login
            </PrimaryButton>
          </IonCardContent>
        </IonCard>
        <div>
          <h5 style={{ textAlign: 'center', color: 'grey' }}>
            <IonButton
              style={{ color: 'var(--ion-primary-color' }}
              color="clear"
              onClick={async () => {
                setTerms('pp');
              }}
            >
              Privacy Policy
            </IonButton>
          </h5>
          <h5 style={{ textAlign: 'center', color: 'grey' }}>
            <IonButton
              style={{ color: 'var(--ion-primary-color' }}
              color="clear"
              onClick={async () => {
                setTerms('terms');
              }}
            >
              Terms of Use
            </IonButton>
          </h5>
          <h6 style={{ textAlign: 'center', color: 'grey' }}>1.3.42</h6>
          {/* <h6 style={{ textAlign: 'center', color: '#3880ff' }}>
            © 2020 Saudi Arabian Oil Company
          </h6> */}
        </div>
        <IonAlert
          isOpen={!!showToast}
          onDidDismiss={() => setShowToast('')}
          // duration={2000}
          message={showToast}
          buttons={['OK']}
        />

        <IonModal isOpen={testing}>
          <TestingPage isNotLoggedIn cancel={() => setTesting(false)} />
        </IonModal>
        <IonModal
          backdropDismiss
          onDidDismiss={() => setTerms('')}
          isOpen={!!terms}
        >
          <div>
            <IonIcon
              icon={closeCircleOutline}
              style={{
                position: 'absolute',
                right: '10px',
                top: '20px',
                fontSize: '2rem',
                zIndex: '1000',
              }}
              onClick={() => setTerms('')}
            />
            <div style={{ overflow: 'scroll' }}>
              <Document
                file={`assets/${terms}.pdf`}
                onLoadSuccess={onDocumentLoadSuccess}
              >
                {Array.from(new Array(numPages), (el, index) => (
                  <Page key={`page_${index + 1}`} pageNumber={index + 1} />
                ))}
              </Document>
            </div>
          </div>
        </IonModal>
      </IonContent>
    </IonPage>
  );
}
