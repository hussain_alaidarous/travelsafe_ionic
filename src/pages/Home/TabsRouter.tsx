import React from 'react';
import { IonTabs, IonRouterOutlet, IonTabBar, IonTabButton, IonIcon, IonLabel } from '@ionic/react';
import { Route, Redirect } from 'react-router';
import Tab1 from './Tab1';
import Tab2 from './Tab2';
import Tab3 from './Tab3';
import Tab4 from './Tab4';
import { calendar, locationSharp, car } from 'ionicons/icons';

export default function TabsRouter() {
	return (
		<IonTabs>
			<IonRouterOutlet>
				<Route path="/home/tab1" component={Tab1} />
				<Route path="/home/tab2" component={Tab2} />
				<Route path="/home/tab3" component={Tab3} />
				<Route path="/home/tab4" component={Tab4} />
				<Route path="/home" render={() => <Redirect to="/home/tab1" />} exact={true} />
			</IonRouterOutlet>
			<IonTabBar slot="bottom">
				<IonTabButton tab="tab1" href="/home/tab1">
					<IonIcon icon={car} />
					<IonLabel>Journeys</IonLabel>
				</IonTabButton>
				<IonTabButton tab="tab2" href="/home/tab2">
					<IonIcon icon={locationSharp} />
					<IonLabel>Active</IonLabel>
				</IonTabButton>
				<IonTabButton tab="tab3" href="/home/tab3">
					<IonIcon icon={calendar} />
					<IonLabel>Plan</IonLabel>
				</IonTabButton>
				{/* <IonTabButton tab="tab4" href="/home/tab4">
					<IonIcon icon={settings} />
					<IonLabel>Tester</IonLabel>
				</IonTabButton> */}
			</IonTabBar>
		</IonTabs>
	);
}
