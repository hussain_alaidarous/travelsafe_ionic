import React, { useState, useContext, useEffect } from 'react';
import {
	IonPage,
	IonIcon,
	IonContent,
	IonItem,
	IonLabel,
	IonModal,
	IonDatetime,
	IonList,
	IonText,
	IonLoading,
	IonAlert,
	IonRadioGroup,
	IonRadio,
	IonPopover,
	useIonViewDidEnter,
} from '@ionic/react';
import { navigate, location, mapOutline, informationCircle } from 'ionicons/icons';
import '../NewJourney/RoutineJourney.css';
import { useHistory } from 'react-router';
import { JourneyContext, JourneyLocation } from '../../services/JourneyContext';
import { SelectFromMap } from '../NewJourney/SelectFromMap';
import { Header } from '../../components/Header';
import { PrimaryButton } from '../../components/Buttons';
import { SearchLocations } from '../NewJourney/SearchLocations';
import Axios from 'axios';
import { WLS_CONFIG, WLS_PROXY, WLS_JOURNEY_CHECK, WLS_USER_INFO } from '../../services/Constants';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';

export default function Tab3() {
	const history = useHistory();
	const context = useContext(JourneyContext);
	const [ info, setInfo ] = useState({ showPopover: false, event: undefined });
	const [ loading, setLoading ] = useState(false);
	const [ alert, setAlert ] = useState('');
	const [ toastMessage, setToastMessage ] = useState('');
	const [ routeConfig, setRouteConfig ] = useState<any>();
	const [ distanceInKm, setDistanceInKm ] = useState(0);
	const [ estimatedTime, setEstimatedTime ] = useState(0);
	const [ departureDate, setDepartureDate ] = useState<Date>();
	const [ arrivalDate, setArrivalDate ] = useState<Date>();
	const [ origin, setOrigin ] = useState<JourneyLocation>({
		name: '',
		geometry: {
			x: 0,
			y: 0,
			spatialReference: {
				wkid: 4326,
			},
		},
	});
	const [ destination, setDestination ] = useState<JourneyLocation>({
		name: '',
		geometry: {
			x: 0,
			y: 0,
			spatialReference: {
				wkid: 4326,
			},
		},
	});
	const [ map, setMap ] = useState<'' | 'origin' | 'destination'>('');
	const [ loadMap, setLoadMap ] = useState(false);
	const [ search, setSearch ] = useState<'' | 'origin' | 'destination'>('');
	const getCurrentPosition = async () => {
		const coordinates = await BackgroundGeolocation.getCurrentLocation();
		setLocationFromModal(
			{
				lat: coordinates.latitude,
				lng: coordinates.longitude,
			},
			`${coordinates.latitude.toFixed(5)}, ${coordinates.longitude.toFixed(5)}`,
			'origin',
		);
	};

	const getDuration = (minutes) => {
		// const seconds = (journey.arrivalDate - journey.departureDate) / 1000;
		const days = Math.floor(minutes / (60 * 24));
		const hours = Math.floor((minutes - days * 60 * 24) / 60);
		const mins = Math.floor(minutes - days * 60 * 24 - hours * 60);
		let temp = '';
		if (days) temp += days + ' days ';
		if (hours) temp += hours + ' hours ';
		if (mins) temp += mins + ' mins ';
		return temp;
	};

	const updateArrivalField = (estimatedTimeInput, departureDate) => {
		if (departureDate) {
			const length = typeof estimatedTimeInput === 'string' ? parseInt(estimatedTimeInput) : estimatedTimeInput;
			const arrival = new Date(departureDate.getTime() + (length + 2) * 60 * 1000);
			setArrivalDate(arrival);
		}
	};

	const setLocationFromModal = async (location, label, source) => {
		const data = {
			name: label,
			geometry: {
				x: location.lng,
				y: location.lat,
				spatialReference: {
					wkid: 4326,
				},
			},
		};
		if (source === 'origin') {
			setOrigin(data);
		} else if (source === 'destination') {
			setDestination(data);
		}

		if (
			(origin.geometry.x && origin.geometry.y && source === 'destination') ||
			(destination.geometry.x && destination.geometry.y && source === 'origin')
		) {
			setLoading(true);
			const originGeometry = source === 'origin' ? data.geometry : origin.geometry;
			const destinationGeometry = source === 'destination' ? data.geometry : destination.geometry;
			const originName = source === 'origin' ? data.name : origin.name;
			const destinationName = source === 'destination' ? data.name : destination.name;
			const stops = {
				type: 'features',
				features: [
					{
						geometry: originGeometry,
						attributes: {
							CurbApproach: null,
							TimeWindowStart: null,
							TimeWindowEnd: null,
							Name: originName,
						},
					},
					{
						geometry: destinationGeometry,
						attributes: {
							CurbApproach: null,
							TimeWindowStart: null,
							TimeWindowEnd: null,
							Name: destinationName,
						},
					},
				],
				doNotLocateOnRestrictedElements: true,
			};
			try {
				const res = await Axios.get(
					`${WLS_PROXY}?${routeConfig.url}/solve?&f=json&stops=${encodeURI(JSON.stringify(stops))}`,
				);
				const data = res.data.directions[0].summary;
				setDistanceInKm(data.totalLength.toFixed(0));
				setEstimatedTime(data.totalDriveTime.toFixed(0));
				updateArrivalField(data.totalDriveTime, departureDate);
			} catch (ex) {
				setAlert(
					'Estimated Distance and Drive Time could not be calculated. Please enter the Arrival Time manually.',
				);
				setDistanceInKm(0);
				setEstimatedTime(0);
			}
			setLoading(false);
		}
	};
	useIonViewDidEnter(() => {
		const userData = localStorage.getItem('userData');
		if (userData) {
			const jsonData = JSON.parse(userData);
			Axios.get(`${WLS_USER_INFO}?networkId=${jsonData.networkId}`, {
				auth: {
					username: jsonData.networkId,
					password: jsonData.password,
				},
			}).then(() => {
				Axios.defaults.auth = {
					username: jsonData.networkId,
					password: jsonData.password,
				};
			});
		}
	});
	useEffect(() => {
		Axios.get(WLS_CONFIG).then((configRes) => {
			setRouteConfig(configRes.data.data.journeyConfig.RouteServiceConfig);
		});
	}, []);
	return (
		<IonPage>
			<Header logoutButton />
			<IonContent>
				<IonList style={{ padding: '0 20px' }}>
					<h6 style={{ padding: '5px' }}>
						What type of journey are you taking?{' '}
						<IonIcon
							icon={informationCircle}
							color="primary"
							onClick={(e) => {
								e.persist();
								setInfo({ showPopover: true, event: e as any });
							}}
						/>
					</h6>
					<IonRadioGroup
						value={context.journey.journeyType.id}
						onIonChange={(event) => context.setJourneyType(parseInt(event.detail.value))}
					>
						<IonItem>
							<IonRadio mode="md" value={2} />
							<IonLabel style={{ marginLeft: '10px' }}>Routine</IonLabel>
						</IonItem>
						<IonItem>
							<IonRadio mode="md" value={1} />
							<IonLabel style={{ marginLeft: '10px' }}>Non-Routine</IonLabel>
						</IonItem>
					</IonRadioGroup>
				</IonList>
				<IonList style={{ padding: '20px' }}>
					<h6 style={{ padding: '5px' }}>Enter your journey details</h6>
					<IonItem>
						<IonIcon onClick={() => setSearch('origin')} color="primary" icon={location} slot="start" />
						<IonText
							onClick={() => setSearch('origin')}
							style={{ width: '100%', color: origin.name ? 'black' : 'grey' }}
						>
							{origin.name || 'Origin'}
						</IonText>
						<IonIcon color="primary" icon={navigate} slot="end" onClick={() => getCurrentPosition()} />
						<IonIcon color="primary" icon={mapOutline} slot="end" onClick={() => setMap('origin')} />
					</IonItem>
					<IonItem>
						<IonIcon
							onClick={() => setSearch('destination')}
							color="primary"
							icon={location}
							slot="start"
						/>
						<IonText
							onClick={() => setSearch('destination')}
							style={{ width: '100%', color: destination.name ? 'black' : 'grey' }}
						>
							{destination.name || 'Destination'}
						</IonText>
						<IonIcon color="primary" icon={mapOutline} slot="end" onClick={() => setMap('destination')} />
					</IonItem>

					<div style={{ marginTop: '20px' }}>
						<IonItem>
							<IonLabel style={{ maxWidth: '100px' }}>Departure:</IonLabel>
							<IonDatetime
								onIonChange={(event) => {
									const date = new Date(event.detail.value!);
									if (date.getTime() < new Date().getTime()) {
										setToastMessage('Departure date cannot be in the past');
									}
									setDepartureDate(date);
									updateArrivalField(estimatedTime, date);
								}}
								placeholder="Select date & time"
								// minuteValues="0,15,30,45"
								displayFormat="DD MMM h:mm a"
							/>
						</IonItem>
						<IonItem>
							<IonLabel style={{ maxWidth: '100px' }}>Arrival:</IonLabel>
							<IonDatetime
								// disabled={!departureDate}
								value={arrivalDate ? arrivalDate.toUTCString() : ''}
								placeholder={departureDate ? 'Select date & time' : 'Expected time'}
								onIonChange={(event) => {
									const date = new Date(event.detail.value!);
									if (date.getTime() < departureDate!.getTime() + 60 * 1000) {
										setToastMessage('Arrival must be after departure');
									}
									if (date.getTime() < departureDate!.getTime() + estimatedTime * 60 * 1000) {
										setToastMessage('Arrival must take longer than estimated drive time');
									}
									setArrivalDate(date);
								}}
								// minuteValues="0,15,30,45"
								displayFormat="DD MMM h:mm a"
							/>
						</IonItem>
					</div>

					<div style={{ marginTop: '20px' }}>
						<IonItem>
							<IonLabel>Estimated Distance: </IonLabel>
							<IonText>{distanceInKm ? `${distanceInKm} km` : 'Not Calculated'} </IonText>
						</IonItem>
						<IonItem>
							<IonLabel>Estimated Drive Time: </IonLabel>
							<IonText>{estimatedTime ? `${getDuration(estimatedTime)}` : 'Not Calculated'}</IonText>
						</IonItem>
					</div>
					<IonModal
						onDidPresent={() => {
							setLoadMap(true);
						}}
						isOpen={!!map}
					>
						{loadMap && (
							<SelectFromMap
								initialMarker={
									map === 'origin' ? (
										{
											lng: origin.geometry.x,
											lat: origin.geometry.y,
											label: origin.name,
										}
									) : (
										{
											lng: destination.geometry.x,
											lat: destination.geometry.y,
											label: destination.name,
										}
									)
								}
								setLocation={async (location, label) => {
									setSearch('');
									setLocationFromModal(location, label, map);
								}}
								close={() => {
									setMap('');
									setSearch('');
									setLoadMap(false);
								}}
							/>
						)}
					</IonModal>
					<div style={{ marginTop: '10px', display: 'flex', width: '100%' }}>
						{/* <ClearButton
						onClick={() => history.goBack()}
						style={{
							margin: 'auto',
						}}
					>
						Back
					</ClearButton> */}
						<PrimaryButton
							disabled={
								!origin.name ||
								!destination.name ||
								!departureDate ||
								!arrivalDate ||
								departureDate.getTime() < new Date().getTime() ||
								arrivalDate.getTime() < departureDate.getTime() + 60 * 1000 ||
								arrivalDate.getTime() < departureDate!.getTime() + estimatedTime * 60 * 1000
							}
							// routerLink={`/journey/new/type`}
							style={{
								margin: 'auto',
							}}
							size="large"
							onClick={async () => {
								context.setJourneyLocationsAndDates(
									origin,
									destination,
									departureDate!.getTime(),
									arrivalDate!.getTime(),
									distanceInKm,
								);
								if (context.journey.journeyType.type === 'Non Routine') {
									history.push('/journey/new/type');
									return;
								}
								setLoading(true);
								try {
									const checkJourneyStatusJson = {
										departureDate: departureDate,
										arrivalDate: arrivalDate,
										geometry: destination.geometry,
									};
									const res = await Axios.get(
										`${WLS_JOURNEY_CHECK}?checkJourneyStatusJson=${JSON.stringify(
											checkJourneyStatusJson,
										)}`,
									);
									setLoading(false);
									if (res.data.data.id === 1) {
										// console.log(history.location);
										history.push('/journey/new/details');
									} else {
										context.setJourneyType(1);
										history.push('/journey/new/type');
									}
								} catch (error) {
									setLoading(false);
									history.push('/journey/new/type');
								}

								// history.push('/journey/new/type');
							}}
						>
							Next
						</PrimaryButton>
					</div>
				</IonList>

				<IonAlert
					mode="ios"
					isOpen={!!toastMessage}
					message={toastMessage}
					onDidDismiss={() => setToastMessage('')}
					buttons={[ 'OK' ]}
				/>
				{/* <IonToast
					message={toastMessage}
					onDidDismiss={() => setToastMessage('')}
					duration={3000}
					position="middle"
					isOpen={!!toastMessage}
				/> */}
				<IonLoading isOpen={loading} />
				{!!search && (
					<SearchLocations
						close={() => setSearch('')}
						setLocation={async (location, label) => {
							setLocationFromModal(location, label, search);
						}}
					/>
				)}
				<IonPopover
					event={info.event}
					backdropDismiss
					onDidDismiss={() => setInfo({ showPopover: false, event: undefined })}
					isOpen={info.showPopover}
				>
					<div style={{ padding: '10px' }}>
						Note: Check your organization’s Journey Management Program for specifics on planning the
						journey.
					</div>
				</IonPopover>
				<IonAlert message={alert} isOpen={!!alert} onDidDismiss={() => setAlert('')} buttons={[ 'OK' ]} />
			</IonContent>
		</IonPage>
	);
}
