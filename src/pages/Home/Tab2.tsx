import React, { useState, useEffect } from 'react';
import {
	IonContent,
	IonPage,
	useIonViewDidEnter,
	IonToast,
	IonText,
	IonIcon,
	IonFabButton,
	IonFab,
} from '@ionic/react';
import './Tab2.css';
import { Journey } from '../../services/JourneyContext';
import JourneyActive from '../StartJourney/JourneyActive';

import { WLS_USER_POSITION, WLS_USER_JOURNEY } from '../../services/Constants';
import JourneySOS from '../StartJourney/JourneySOS';
import {
	BackgroundGeolocation,
	BackgroundGeolocationConfig,
	BackgroundGeolocationEvents,
} from '@ionic-native/background-geolocation';
import { map, add } from 'ionicons/icons';
import { Header } from '../../components/Header';
import Axios from 'axios';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Vibration } from '@ionic-native/vibration';
import { useHistory } from 'react-router';

const Tab2 = () => {
	const history = useHistory();
	const [ journey, setJourney ] = useState<Journey>();
	const [ SOS, setSOS ] = useState('');
	const [ showToast, setShowToast ] = useState('');

	const scheduleNotification = (arrival: number, journeyId) => {
		if (arrival > new Date().getTime()) {
			LocalNotifications.schedule({
				id: journeyId,
				foreground: true,
				trigger: {
					at: new Date(arrival),
				},
				title: 'You Are Late!',
				text: 'Please update your arrival time',
			});
		}
	};

	const getJourneyInfo = async () => {
		const active = localStorage.getItem('active_journey');
		if (!active) {
			setJourney(undefined);
			return;
		}
		let active_journey: Journey = JSON.parse(active);
		Axios.get(`${WLS_USER_JOURNEY}?journeyId=${active_journey.journeyId}`)
			.then((res) => {
				// const list: Journey[] = res.data.data;
				// const selected = list.find((single) => single.journeyId === active_journey.journeyId);
				active_journey = res.data.data;
			})
			.finally(() => {
				if (!active_journey || active_journey.journeyStatus.status === 'Completed') {
					localStorage.removeItem('active_journey');
					setJourney(undefined);
					return;
				}
				if (active_journey.journeyStatus.status === 'SOS') {
					localStorage.setItem('SOS_status', 'confirmed');
					setSOS('confirmed');
				} else {
					localStorage.removeItem('SOS_status');
					setSOS('');
				}
				setJourney(active_journey);
				scheduleNotification(active_journey.arrivalDate, active_journey.journeyId);
			});
	};

	useEffect(
		() => {
			if (journey) {
				LocalNotifications.cancel(journey.journeyId).finally(() => {
					scheduleNotification(journey.arrivalDate, journey.journeyId);
				});
			}
		},
		[ journey ],
	);

	useIonViewDidEnter(() => {
		const active = localStorage.getItem('active_journey');
		if (active) {
			BackgroundGeolocation.checkStatus().then((status) => {
				if (!status.isRunning) {
					const config: BackgroundGeolocationConfig = {
						url: WLS_USER_POSITION,
						activityType: 'AutomotiveNavigation',
						desiredAccuracy: 0,
						stationaryRadius: 50,
						distanceFilter: 50,
						interval: 120000,
						// notificationsEnabled: false,
						debug: false, //  enable this hear sounds for background-geolocation life-cycle.
						stopOnTerminate: true, // enable this to clear background location settings when the app terminates
						postTemplate: {
							journeyId: JSON.parse(active).journeyId,
							userPositions: [
								{
									positionDate: '@time',
									geometry: {
										x: '@longitude',
										y: '@latitude',
										spatialReference: {
											wkid: 4326,
										},
									},
								},
							],
						},
						// startForeground: true,
					};

					BackgroundGeolocation.on(BackgroundGeolocationEvents.location).subscribe(async (data) => {
						if (journey && journey.arrivalDate > new Date().getTime()) {
							Vibration.vibrate([ 2000, 1000, 2000 ]);
						}
						const locationData = {
							journeyId: JSON.parse(active).journeyId,
							userPositions: [
								{
									positionDate: data.time.toFixed(0),
									geometry: {
										x: data.longitude,
										y: data.latitude,
										spatialReference: {
											wkid: 4326,
										},
									},
								},
							],
						};
						await Axios.post(WLS_USER_POSITION, locationData);
					});
					BackgroundGeolocation.configure(config);
					BackgroundGeolocation.start().then(() => setShowToast('Started to watch you location'));
				}
			});
			let active_journey: Journey = JSON.parse(active);
			Axios.get(`${WLS_USER_JOURNEY}?journeyId=${active_journey.journeyId}`)
				.then((res) => {
					// const list: Journey[] = res.data.data;
					// const selected = list.find((single) => single.journeyId === active_journey.journeyId);
					active_journey = res.data.data;
				})
				.finally(() => {
					if (!active_journey) {
						localStorage.removeItem('active_journey');
						if (journey) {
							LocalNotifications.cancel(journey.journeyId);
						}
						return;
					}
					if (active_journey.journeyStatus.status === 'SOS') {
						localStorage.setItem('SOS_status', 'confirmed');
						setSOS('confirmed');
					} else {
						localStorage.removeItem('SOS_status');
						setSOS('');
					}
					setJourney(active_journey);
					scheduleNotification(active_journey.arrivalDate, active_journey.journeyId);
				});
			setInterval(() => getJourneyInfo(), 30000);
		} else {
			setJourney(undefined);
		}
		const sos = localStorage.getItem('SOS_status');
		if (sos) {
			setSOS(sos);
			return;
		}
	});

	return (
		<IonPage>
			<Header logoutButton />
			<IonContent>
				<IonToast
					isOpen={!!showToast}
					onDidDismiss={() => setShowToast('')}
					duration={1000}
					message={showToast}
				/>
				{journey &&
				!SOS && (
					<JourneyActive
						stopJourney={() => {
							BackgroundGeolocation.stop();
						}}
						initaiteSOS={() => {
							localStorage.setItem('SOS_status', 'initiated');
							setSOS('initiated');
						}}
						journey={journey}
						setJourney={setJourney}
					/>
				)}
				{journey &&
				SOS && (
					<JourneySOS
						journey={journey}
						stopJourney={() => {
							BackgroundGeolocation.stop();
							localStorage.removeItem('active_journey');
						}}
						cancelSOS={() => {
							setSOS('');
							localStorage.removeItem('SOS_status');
						}}
					/>
				)}
				{!journey && (
					<div>
						<div
							style={{
								margin: '50% 0 20px 0',
								fontSize: '3rem',
								textAlign: 'center',
								color: '#dbdbdb',
							}}
						>
							<IonIcon icon={map} />
						</div>
						<div
							style={{
								margin: '0 0 40px 0',
								fontSize: '1.5rem',
								textAlign: 'center',
								color: '#75787b',
							}}
						>
							<IonText>Start a journey to monitor it here</IonText>
						</div>
						{/* <div style={{ display: 'flex' }}>
							<IonButton
								onClick={() => {
									history.push('/journey/new');
									window.location.reload();
								}}
								style={{ margin: 'auto' }}
							>
								<IonText style={{ padding: '20px' }}>Create</IonText>
							</IonButton>
						</div> */}
					</div>
				)}

				{!journey && (
					<IonFab vertical="bottom" horizontal="end" slot="fixed">
						<IonFabButton
							color="travel"
							onClick={() => {
								history.push('/home/tab3');
								// window.location.reload();
							}}
						>
							<IonIcon icon={add} />
						</IonFabButton>
					</IonFab>
				)}
			</IonContent>
		</IonPage>
	);
};

export default Tab2;
