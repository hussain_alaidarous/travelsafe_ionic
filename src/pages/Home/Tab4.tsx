import React from 'react';
import { IonContent, IonPage } from '@ionic/react';
import './Tab4.css';

import { Header } from '../../components/Header';
import { TestingPage } from '../../components/TestingPage';

const Tab4: React.FC = () => {
  return (
    <IonPage>
      <Header logoutButton />
      <IonContent>
        <TestingPage />
      </IonContent>
    </IonPage>
  );
};

export default Tab4;
