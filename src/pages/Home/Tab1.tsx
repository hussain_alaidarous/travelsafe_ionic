import React, { useState, useEffect } from 'react';
import {
  IonContent,
  IonLoading,
  IonPage,
  IonFab,
  IonFabButton,
  IonIcon,
  IonText,
  IonActionSheet,
  IonAlert,
  IonButton,
  useIonViewDidEnter,
} from '@ionic/react';
import './Tab1.css';
import { JourneyCard } from '../../components/JourneyCard';
import {
  add,
  map,
  trash,
  car,
  informationCircle,
  star,
  starOutline,
} from 'ionicons/icons';
import {
  WLS_USER_JOURNEYS,
  WLS_CANCEL_JOURNEY,
} from '../../services/Constants';
import { Journey } from '../../services/JourneyContext';
import { useHistory } from 'react-router';
import { Header } from '../../components/Header';
import Axios from 'axios';
import { LocalNotifications } from '@ionic-native/local-notifications';

const Tab1 = (props) => {
  const [showLoading, setShowLoading] = useState(false);
  const [showToast, setShowToast] = useState('');
  const [journies, setJournies] = useState<Journey[]>([]);
  const [journeyList, setjourneyList] = useState<Journey[]>([]);
  const [favoriteList, setFavoriteList] = useState<number[]>([]);
  const [filter, setFilter] = useState({
    Approved: true,
    'Pending Approval': true,
    Active: false,
    Completed: false,
    Canceled: false,
    Terminated: false,
    Late: false,
    Rejected: false,
    SOS: false,
    Expired: false,
  });
  // const [ filter, setFilter ] = useState(true);
  const [actionButtons, setActionButtons] = useState([] as any[]);
  const [alert, setAlert] = useState('');
  const [selectedJourney, setSelectedJourney] = useState<Journey>();

  const history = useHistory();
  useIonViewDidEnter(() => {
    // if (props.location.pathname !== '/home/tab1') {
    // 	return;
    // }
    setShowLoading(true);
    Axios.get(WLS_USER_JOURNEYS)
      .then((res) => {
        res.data.data.forEach((journey) => {
          if (
            journey.journeyStatus.status === 'Active' ||
            journey.journeyStatus.status === 'Late'
          ) {
            localStorage.setItem('active_journey', JSON.stringify(journey));
            // history.push('/home/tab2');
          }
        });
        const favorite = localStorage.getItem('favorite_journeys');
        // const wantedJournies = filter ? filtered : [ ...filtered, ...nonFiltered ];
        if (favorite) {
          const list: number[] = JSON.parse(favorite);
          const favoriteList = res.data.data.filter((single) =>
            list.includes(single.journeyId),
          );
          const nonFavoriteList = res.data.data.filter(
            (single) => !list.includes(single.journeyId),
          );
          setFavoriteList(list);
          setJournies([...favoriteList, ...nonFavoriteList]);
        } else {
          setJournies(res.data.data);
        }
        res.data.data
          .filter((rec) => rec.journeyStatus.status === 'Approved')
          .map((rec) => {
            return LocalNotifications.schedule({
              id: rec.journeyId,
              foreground: true,
              trigger: {
                at: new Date(rec.departureDate),
              },
              title: 'You have a scheduled journey',
              text: `Don't forget to start your scheduled journey`,
            });
          });
      })
      .catch((error) => console.log(error))
      .finally(() => setShowLoading(false));
  });

  useEffect(() => {
    const filtered = journies.filter((journey) =>
      Object.keys(filter)
        .filter((status) => filter[status])
        .includes(journey.journeyStatus.status),
    );
    const sorted: Journey[] = filtered.sort(
      (a: Journey, b: Journey) => b.departureDate - a.departureDate,
    );
    setjourneyList(sorted);
  }, [filter, journies]);

  const showActionSheet = (journey: Journey) => {
    const buttons = [] as any[];
    const { status } = journey.journeyStatus;
    if (
      status === 'Approved' &&
      journey.departureDate <= new Date().getTime() + 3 * 1000 * 60 * 60
    ) {
      buttons.push({
        text: 'Start Journey',
        icon: car,
        handler: () => {
          const isActive = localStorage.getItem('active_journey');
          if (isActive) {
            setAlert('You cannot start a journey while another one is active');
            return;
          }
          history.push(`/journey/start/checklist/${journey.journeyId}`);
        },
      });
    }

    buttons.push({
      text: 'View Journey',
      icon: informationCircle,
      handler: () => {
        history.push(`/journey/start/info/${journey.journeyId}`);
      },
    });
    if (
      status !== 'Completed' &&
      status !== 'Canceled' &&
      status !== 'SOS' &&
      status !== 'Expired'
    ) {
      buttons.push({
        text: 'Cancel Journey',
        role: 'destructive',
        icon: trash,
        handler: () => {
          setSelectedJourney(journey);
        },
      });
    }
    let list: number[] = [];
    const favorite = localStorage.getItem('favorite_journeys');
    if (!favorite) {
      localStorage.setItem('favorite_journeys', JSON.stringify([]));
      const isFavorite = list.includes(journey.journeyId);
      if (isFavorite) {
      } else {
        list.push(journey.journeyId);
        localStorage.setItem('favorite_journeys', JSON.stringify(list));
      }
    } else {
      list = JSON.parse(favorite);
    }
    const isFavorite = list.includes(journey.journeyId);

    buttons.push(
      isFavorite
        ? {
            text: 'Remove Favorite',
            icon: starOutline,
            handler: () => {
              list.splice(list.indexOf(journey.journeyId), 1);
              localStorage.setItem('favorite_journeys', JSON.stringify(list));
              setFavoriteList(list);
            },
          }
        : {
            text: 'Add Favorite',
            icon: star,
            handler: () => {
              list.push(journey.journeyId);
              localStorage.setItem('favorite_journeys', JSON.stringify(list));
              setFavoriteList(list);
            },
          },
    );

    setActionButtons(buttons);
  };
  return (
    <IonPage>
      <Header logoutButton testButton />
      <IonContent>
        <div
          className="journey-filter"
          style={{
            margin: '20px',
            position: 'relative',
            display: 'flex',
            overflowX: 'scroll',
          }}
        >
          {Object.keys(filter).map((status) => {
            return (
              <IonButton
                key={status}
                style={{
                  '--background': filter[status] ? '' : 'white',
                  height: '2rem',
                  padding: '5px',
                }}
                fill={filter[status] ? 'solid' : 'outline'}
                onClick={() =>
                  setFilter({ ...filter, [status]: !filter[status] })
                }
                size="small"
              >
                {status === 'Pending Approval' ? 'Pending' : status}
              </IonButton>
            );
          })}
        </div>
        {/* <IonCard mode="ios" style={{ margin: '20px', position: 'relative' }}>
					<IonItem>
						<IonCheckbox checked={!filter} onIonChange={(event) => setFilter(!event.detail.checked)} />
						<IonLabel style={{ marginLeft: '10px' }}>Show all journeys</IonLabel>
					</IonItem>
				</IonCard> */}
        {journeyList.map((journey) => {
          return (
            <div
              key={journey.journeyId}
              onClick={() => showActionSheet(journey)}
            >
              <JourneyCard
                start={journey.departureDate}
                end={journey.arrivalDate}
                name={journey.journeyName}
                status={journey.journeyStatus.status}
                purpose={journey.purpose}
                isFavorite={favoriteList.includes(journey.journeyId)}
              />
            </div>
          );
        })}
        {journeyList.length === 0 && !showLoading && (
          <div>
            <div
              style={{
                margin: '20% 0 20px 0',
                fontSize: '3rem',
                textAlign: 'center',
                color: '#dbdbdb',
              }}
            >
              <IonIcon icon={map} />
            </div>
            <div
              style={{
                margin: '0 0 40px 0',
                fontSize: '1.5rem',
                textAlign: 'center',
                color: '#75787b',
              }}
            >
              <IonText>You have no upcoming journeys!</IonText>
            </div>
            {/* <div style={{ display: 'flex' }}>
							<IonButton
								onClick={() => {
									history.push('/home/tab3');
									// window.location.reload();
								}}
								style={{ margin: 'auto' }}
							>
								<IonText style={{ padding: '20px' }}>Create</IonText>
							</IonButton>
						</div> */}
          </div>
        )}

        {/* <IonFab vertical="bottom" horizontal="end" slot="fixed">
					<IonFabButton
						color="travel"
						onClick={() => {
							history.push('/home/tab3');
							// window.location.reload();
						}}
					>
						<IonIcon icon={add} />
					</IonFabButton>
				</IonFab> */}
        <IonAlert
          message={alert}
          onDidDismiss={() => setAlert('')}
          isOpen={!!alert}
          buttons={['OK']}
        />
        <IonActionSheet
          onDidDismiss={() => setActionButtons([])}
          isOpen={!!actionButtons.length}
          mode="ios"
          buttons={actionButtons}
        />

        <IonLoading
          isOpen={showLoading}
          onDidDismiss={() => setShowLoading(false)}
          message={'Please wait...'}
          duration={5000}
        />
        <IonAlert
          isOpen={!!showToast}
          message={showToast}
          onDidDismiss={() => setShowToast('')}
          buttons={['OK']}
        />
        <IonAlert
          isOpen={!!selectedJourney}
          onDidDismiss={() => setSelectedJourney(undefined)}
          // header={'Are you sure?'}
          subHeader="Please enter your reason"
          inputs={[
            {
              name: 'reason',
              type: 'text',
              placeholder: 'Please enter your reason',
            },
          ]}
          buttons={[
            {
              text: 'Back',
              role: 'cancel',
            },
            {
              text: 'Proceed',
              cssClass: 'secondary',
              handler: async (data) => {
                if (!data.reason) {
                  setShowToast('You have to provide a reason');
                  return null;
                } else {
                  try {
                    await Axios.post(WLS_CANCEL_JOURNEY, {
                      journeyId: selectedJourney!.journeyId,
                      remark: data.reason,
                    });
                    setAlert('Journey has been Canceled');
                    history.push('/home/tab1');
                  } catch (error) {
                    setAlert('Something went wrong');
                  }
                }
              },
            },
          ]}
        />
      </IonContent>
    </IonPage>
  );
};

export default Tab1;
