import React, { useState, useContext } from 'react';
import {
  IonPage,
  IonContent,
  IonItem,
  IonLabel,
  IonInput,
  IonList,
  IonText,
  IonToast,
  useIonViewDidEnter,
  IonAlert,
  IonRadioGroup,
  IonRadio,
  IonIcon,
} from '@ionic/react';
import { useHistory } from 'react-router';
import { UserContext } from '../../services/UserContext';
import { JourneyContext, JourneyUser } from '../../services/JourneyContext';
import { WLS_USER_INFO } from '../../services/Constants';
import { Header } from '../../components/Header';
import { ClearButton, PrimaryButton } from '../../components/Buttons';
import Axios from 'axios';
import { add } from 'ionicons/icons';

export default function JourneyDetails() {
  const history = useHistory();
  const [journeyName, setJourneyName] = useState('');
  // const [ purpose, setPurpose ] = useState('');
  const [driverId, setDriverId] = useState('');
  const [passengers, setPassengers] = useState(
    [] as { networkId: string; phone: string }[],
  );
  const [phone, setPhone] = useState('');
  const [satellite, setSatellite] = useState('');
  const [serverError, setServerError] = useState('');
  const [duration, setDuration] = useState('');
  const [withPassengers, setWithPassengers] = useState();
  const [passengerError, setPassengerError] = useState(false);
  const user = useContext(UserContext);
  const context = useContext(JourneyContext);

  useIonViewDidEnter(() => {
    const seconds =
      (context.journey.arrivalDate - context.journey.departureDate) / 1000;
    const days = Math.floor(seconds / (60 * 60 * 24));
    const hours = Math.floor((seconds - days * 60 * 60 * 24) / (60 * 60));
    const mins = Math.floor(
      (seconds - days * 60 * 60 * 24 - hours * 60 * 60) / 60,
    );
    let temp = '';
    if (days) temp += days + ' days ';
    if (hours) temp += hours + ' hours ';
    if (mins) temp += mins + ' mins ';
    setDuration(temp);
    if (!driverId) {
      const initDriver = JSON.parse(localStorage.getItem('userData')!);
      setDriverId(initDriver.networkId);
      if (initDriver.phone) {
        setPhone(initDriver.phone);
      }
    }
  });

  return (
    <IonPage>
      <Header backButton />
      <IonContent>
        <IonList style={{ padding: '20px' }}>
          <IonItem>
            <IonLabel style={{ fontSize: '1.2rem' }} position="floating">
              Journey Name
            </IonLabel>
            <IonInput
              placeholder="Please enter journey name"
              onIonChange={(event) => setJourneyName(event.detail.value!)}
            />
          </IonItem>
          {/* <IonItem>
						<IonLabel style={{fontSize:'1.2rem'}} position="floating">Journey Purpose</IonLabel>
						<IonInput
							placeholder="Please enter purpose of journey"
							onIonChange={(event) => setPurpose(event.detail.value!)}
						/>
					</IonItem> */}
          <IonItem>
            <IonLabel style={{ fontSize: '1.2rem' }} position="floating">
              Driver (Network ID)
            </IonLabel>
            <IonInput
              placeholder="Please enter Network ID of driver"
              onIonChange={(event) => setDriverId(event.detail.value!)}
              value={driverId}
            />
          </IonItem>
          <IonItem>
            <IonLabel style={{ fontSize: '1.2rem' }} position="floating">
              Mobile Number
            </IonLabel>
            <IonInput
              placeholder="Please enter Mobile Number of driver"
              type="tel"
              value={phone}
              onIonChange={(event) => setPhone(event.detail.value!)}
            />
          </IonItem>
          <IonItem>
            <IonLabel style={{ fontSize: '1.2rem' }} position="floating">
              Satellite Phone
            </IonLabel>
            <IonInput
              placeholder="Please enter Satellite phone number"
              type="tel"
              onIonChange={(event) => setSatellite(event.detail.value!)}
            />
          </IonItem>
          <IonItem>
            <IonText>Duration: {duration}</IonText>
          </IonItem>
          <IonList>
            <IonItem>
              <IonLabel>Are you traveling with passengers?</IonLabel>
            </IonItem>
            <IonRadioGroup
              value={withPassengers}
              onIonChange={(event) => setWithPassengers(event.detail.value)}
            >
              <IonItem>
                <IonRadio mode="md" value={true} />
                <IonLabel style={{ marginLeft: '10px' }}>Yes</IonLabel>
              </IonItem>
              <IonItem>
                <IonRadio mode="md" value={false} />
                <IonLabel style={{ marginLeft: '10px' }}>No</IonLabel>
              </IonItem>
            </IonRadioGroup>
            {withPassengers &&
              passengers.map((rec, index) => {
                return (
                  <React.Fragment key={index}>
                    <IonItem>
                      <IonLabel
                        style={{ fontSize: '1.2rem' }}
                        position="floating"
                      >
                        Passenger network ID
                      </IonLabel>
                      <IonInput
                        placeholder="Please enter passenger Network ID"
                        onIonChange={(event) => {
                          const temp = passengers.map((passenger, index2) => {
                            if (index === index2) {
                              return {
                                networkId: event.detail.value!,
                                phone: passenger.phone,
                                // nonAramco: passenger.nonAramco,
                              };
                            }
                            return passenger;
                          });
                          setPassengers(temp);
                        }}
                      />
                    </IonItem>
                    <IonItem>
                      <IonLabel
                        style={{ fontSize: '1.2rem' }}
                        position="floating"
                      >
                        Passenger Mobile
                      </IonLabel>
                      <IonInput
                        placeholder="Passenger phone number"
                        type="tel"
                        onIonChange={(event) => {
                          const temp = passengers.map((passenger, index2) => {
                            if (index === index2) {
                              return {
                                networkId: passenger.networkId,
                                phone: event.detail.value!,
                                // nonAramco: passenger.nonAramco,
                              };
                            }
                            return passenger;
                          });
                          setPassengers(temp);
                        }}
                      />
                    </IonItem>
                  </React.Fragment>
                );
              })}
            {withPassengers && (
              <IonItem
                disabled={
                  !!passengers.length &&
                  !passengers[passengers.length - 1].networkId
                }
                onClick={() => {
                  setPassengers([
                    ...passengers,
                    {
                      networkId: '',
                      phone: '',
                      // nonAramco: false,
                    },
                  ]);
                }}
                style={{ padding: '0', marginLleft: '-10px' }}
              >
                <IonIcon color="primary" icon={add} />{' '}
                <IonText color="primary" style={{ marginLeft: '10px' }}>
                  Add Passenger
                </IonText>
                {/* </IonButton> */}
              </IonItem>
            )}
          </IonList>

          <div style={{ display: 'flex' }}>
            <ClearButton
              onClick={() => history.goBack()}
              style={{
                margin: 'auto',
              }}
            >
              Back
            </ClearButton>
            <PrimaryButton
              disabled={
                !journeyName ||
                !driverId ||
                !phone ||
                withPassengers === undefined
              }
              style={{
                margin: 'auto',
              }}
              onClick={async () => {
                if (
                  withPassengers &&
                  (!passengers.length || !passengers[0].networkId)
                ) {
                  setPassengerError(true);
                  return;
                }
                const requester = user!;
                const res = (
                  await Axios.get(
                    `${WLS_USER_INFO}?networkId=${driverId.trim()}`,
                  )
                ).data;
                const driver = res.data;

                const passanger_list: JourneyUser[] = [];
                if (withPassengers) {
                  for (let i = 0; i < passengers.length; i++) {
                    const passenger = passengers[i];
                    if (!passenger.networkId.trim()) {
                      continue;
                    }
                    try {
                      const res = (
                        await Axios.get(
                          `${WLS_USER_INFO}?networkId=${passenger.networkId.trim()}`,
                        )
                      ).data;
                      const data = res.data;
                      if (!data) {
                        throw Error('Passenger not found');
                      }
                      passanger_list.push({
                        ...data,
                        mobilePhone: passenger.phone,
                      });
                    } catch (ex) {
                      setServerError(
                        `Passenger: ${passenger.networkId} not found`,
                      );
                      console.log('PassengerInfoError', ex);
                      return;
                    }
                  }
                }
                if (!driver) {
                  setServerError(`Incorrect Driver network ID: ${driverId}`);
                  return;
                }
                const filter = passanger_list.filter((single) => !single);
                if (filter.length) {
                  const err = passanger_list.indexOf(filter[0]);
                  setServerError(
                    `Incorrect Passenger network ID: ${passengers[err]}`,
                  );
                  return;
                }
                const userInfo = JSON.parse(localStorage.getItem('userData')!);
                localStorage.setItem(
                  'userData',
                  JSON.stringify({
                    ...userInfo,
                    phone,
                  }),
                );

                const details = {
                  journeyName,
                  purpose: journeyName,
                  requester,
                  driver: {
                    ...driver,
                    mobilePhone: phone,
                    satellitePhone: satellite,
                  },
                  passengers: passanger_list,
                };
                context.setJourneyDetails(details);
                history.push('/journey/new/summary');
              }}
            >
              Next
            </PrimaryButton>
          </div>
        </IonList>
      </IonContent>
      <IonToast
        isOpen={!!serverError}
        onDidDismiss={() => setServerError('')}
        duration={5000}
        message={serverError}
      />
      <IonAlert
        onWillDismiss={() => setPassengerError(false)}
        isOpen={passengerError}
        message="If you are traveling with passengers, you have to enter at least one."
        buttons={[{ role: 'ok', text: 'OK' }]}
      />
    </IonPage>
  );
}
