import React from 'react';
import { useHistory } from 'react-router';
import { IonPage, IonContent, IonIcon, IonButton } from '@ionic/react';
import { personAdd } from 'ionicons/icons';
import { Header } from '../../components/Header';

export default function JourneyCheck() {
	// const { journey } = useContext(JourneyContext);
	// const [ approval, setApproval ] = useState(0);
	const history = useHistory();
	// useIonViewDidEnter(() => {
	// 	const getStatus = async () => {
	// 		const checkJourneyStatusJson = {
	// 			departureDate: journey.departureDate,
	// 			arrivalDate: journey.arrivalDate,
	// 			geometry: journey.journeyLocations.journeyEnd.geometry,
	// 		};
	// 		try {
	// 			if (journey.journeyType.type === 'Non Routine') {
	// 				setApproval(2);
	// 				return;
	// 			}
	// 			const res = await Axios.get(
	// 				`${WLS_JOURNEY_CHECK}?checkJourneyStatusJson=${JSON.stringify(checkJourneyStatusJson)}`,
	// 			);
	// 			const data = res.data;
	// 			setApproval(data.data.id);
	// 			// if (data.data.id === 1) {
	// 			// 	console.log(history.location)
	// 			// 	history.push('/journey/new/details');
	// 			// }
	// 		} catch (ex) {
	// 			setApproval(2);
	// 		}
	// 		// if (data.data.id !== context.journey.journeyType.id) {
	// 		// 	context.setJourneyType(data.data);
	// 		// }
	// 	};
	// 	getStatus();
	// });
	return (
		<IonPage>
			<Header backButton />
			<IonContent>
				{/* {approval === 0 && <IonLoading isOpen />}
				{approval === 1 && <NoApproval />}
				{approval === 2 && <ApprovalRequired />} */}
				<ApprovalRequired />
				<div style={{ display: 'flex', marginTop: '10px' }}>
					<IonButton
						routerLink={`/journey/new/details`}
						color="primary"
						style={{ margin: 'auto', width: '54%', height: '50px' }}
					>
						Continue with details
					</IonButton>
				</div>
				<div style={{ display: 'flex', marginTop: '20px' }}>
					<IonButton
						color="link"
						style={{
							margin: 'auto',
							width: '54%',
							height: '50px',
							color: 'grey',
						}}
						onClick={() => history.goBack()}
					>
						Cancel
					</IonButton>
				</div>
			</IonContent>
		</IonPage>
	);
}

function ApprovalRequired() {
	return (
		<React.Fragment>
			<div style={{ display: 'flex', marginTop: '40%' }}>
				<div style={{ margin: 'auto' }}>
					<div>
						<IonIcon
							style={{ height: '5rem', width: '5rem', color: '#dbdbdb' }}
							size="large"
							icon={personAdd}
						/>
					</div>
				</div>
			</div>
			<div style={{ textAlign: 'center', padding: '0 20%' }}>
				<p>Your journey is within an area that requires approval or is non-routine</p>
				<p>(Division/Unit managers assigned will approve via workflow)</p>
			</div>
		</React.Fragment>
	);
}

// function NoApproval() {
// 	return (
// 		<React.Fragment>
// 			<div style={{ display: 'flex', marginTop: '20%' }}>
// 				<div style={{ margin: 'auto' }}>
// 					<div>
// 						<IonIcon
// 							style={{ height: '10rem', width: '10rem' }}
// 							size="large"
// 							color="success"
// 							icon={checkmark}
// 						/>

// 						{/* <h4>No Approval Need</h4> */}
// 					</div>
// 				</div>
// 			</div>
// 			<div style={{ textAlign: 'center' }}>
// 				<p>Your journey can be started without approval</p>
// 			</div>
// 		</React.Fragment>
// 	);
// }
