import React, { useContext } from 'react';
import { IonContent, IonPage, IonText, IonIcon } from '@ionic/react';
import './JourneyNew.css';
import { useHistory } from 'react-router';
import { JourneyContext } from '../../services/JourneyContext';
import { map } from 'ionicons/icons';
import { Header } from '../../components/Header';
import { PrimaryButton } from '../../components/Buttons';

export default function JourneyNew() {
	const history = useHistory();
	const context = useContext(JourneyContext);
	return (
		<IonPage>
			<Header backButton />
			<IonContent>
				<div
					style={{
						fontSize: '2rem',
						textAlign: 'center',
						color: '#dbdbdb',
						display: 'inline-flex',
						position: 'relative',
						margin: '10% 0 0 10%',
					}}
				>
					<IonIcon icon={map} style={{ position: 'absolute', top: '5px' }} />
					<IonText style={{ color: '#303030', fontSize: '1rem', margin: '10px 0 0 40px' }}>
						Create New Journey
					</IonText>
					<IonText
						style={{ color: '#14afe5', fontSize: '.8rem', margin: '11px 0 0 40px' }}
						onClick={() => history.goBack()}
					>
						Cancel
					</IonText>
				</div>
				<div
					style={{
						fontSize: '2rem',
						textAlign: 'center',
						color: '#dbdbdb',
						display: 'inline-flex',
						position: 'relative',
						margin: '30px 0 0 10%',
					}}
				>
					<h4 style={{ color: '#303030' }}>What type of journey are you taking?</h4>
				</div>
				<h6 style={{ color: '#303030', margin: '20px 10%' }}>
					Note: Check your organization’s Journey Management Program for specifics on planning the journey.
				</h6>
				<div style={{ position: 'absolute', bottom: '20px', display: 'flex', width: '100%' }}>
					<PrimaryButton
						onClick={() => {
							context.setJourneyType(1);
							history.push('/journey/new/location');
						}}
						style={{ margin: 'auto', width: '120px' }}
					>
						Non Routine
					</PrimaryButton>
					<PrimaryButton
						onClick={() => {
							context.setJourneyType(2);
							history.push('/journey/new/location');
						}}
						style={{ margin: 'auto', width: '120px' }}
					>
						Routine
					</PrimaryButton>
				</div>
			</IonContent>
		</IonPage>
	);
}
