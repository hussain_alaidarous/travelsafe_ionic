import React, { useState, useEffect } from 'react';
import { WebMapView } from '../../components/WebMapViewer';
import { IonButton, IonItem, IonSearchbar, IonList } from '@ionic/react';
import Axios from 'axios';
import { WLS_CONFIG, WLS_PROXY } from '../../services/Constants';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';

interface Props {
	setLocation: (marker, label) => void;
	close: () => void;
	initialMarker?: { lat: number; lng: number; label: string };
}
interface SearchResult {
	layerName: string;
	value: string;
	geometry: {
		x: number;
		y: number;
	};
}

export const SelectFromMap = ({ setLocation, close, initialMarker }: Props) => {
	const [ marker, setMarker ] = useState<any>();
	const [ search, setSearch ] = useState('');
	const [ results, setResults ] = useState<SearchResult[]>([]);
	const [ empty, setEmpty ] = useState(false);
	const [ selected, setSelected ] = useState('');
	const [ searchConfig, setSearchConfig ] = useState<any>();
	const [ geometryConfig, setGeometryConfig ] = useState<any>();
	const [ center, setCenter ] = useState([ 26.2985261, 50.1115937 ]);
	const [ zoom, setZoom ] = useState(4);
	const getResults = async (searchText) => {
		const res = await Axios.get(
			`${WLS_PROXY}?${searchConfig.url}/find?searchText=${searchText}&layers=${searchConfig.layers}&f=json`,
		);
		const obj = {};
		const results: SearchResult[] = res.data.results.map((result) => ({
			layerName: result.layerName,
			value: result.value,
			geometry: result.geometry,
		}));
		results.forEach((result) => {
			obj[result.value.toUpperCase()] = result;
		});
		const data = Object.keys(obj)
			.map((key) => obj[key])
			.filter((rec, index) => index < 10 && !rec.value.startsWith('http'));
		setEmpty(!data.length);
		setResults(data);
	};
	useEffect(
		() => {
			if (initialMarker && initialMarker.label && !selected) {
				setSelected(initialMarker.label);
				setCenter([ initialMarker.lat, initialMarker.lng ]);
				setZoom(12);
			} else {
				BackgroundGeolocation.getCurrentLocation().then((location) => {
					if (initialMarker && initialMarker.label) {
						return;
					}

					setCenter([ location.latitude, location.longitude ]);
					setZoom(13);
				});
			}
			Axios.get(WLS_CONFIG).then((configRes) => {
				setSearchConfig(configRes.data.data.journeyConfig.SearchLayersConfig);
				setGeometryConfig(configRes.data.data.journeyConfig.GeometryServiceConfig);
			});
		}, // eslint-disable-next-line
		[ initialMarker ],
	);
	return (
		<React.Fragment>
			<div style={{ position: 'absolute', zIndex: 2, width: '100%' }}>
				<IonSearchbar
					style={{ marginTop: '30px' }}
					mode="md"
					value={search}
					debounce={700}
					placeholder={selected || 'Search Aramco Maps'}
					onIonChange={(event) => {
						setSearch(event.detail.value!);
						if (event.detail.value && event.detail.value.length > 2) {
							getResults(event.detail.value);
						}
					}}
				/>
				{search.length >= 3 && (
					<IonList>
						{empty && <IonItem>No Results for search</IonItem>}
						{results.map((result) => (
							<IonItem
								onClick={async () => {
									const data = {
										geometryType: 'esriGeometryPoint',
										geometries: [ { x: result.geometry.x, y: result.geometry.y } ],
									};
									const res = await Axios.get(
										`${WLS_PROXY}?${geometryConfig.url}/project?inSR=${geometryConfig.inSR}&outSR=${geometryConfig.outSR}&geometries=${encodeURI(
											JSON.stringify(data),
										)}&f=json`,
									);
									const { x, y } = res.data.geometries[0];
									setMarker({
										lat: y,
										lng: x,
									});
									setSearch('');
									setSelected(result.value);
									setZoom(searchConfig.zoom);
									setCenter([ y, x ]);
								}}
								key={result.value}
							>
								{result.layerName}: {result.value}
							</IonItem>
						))}
					</IonList>
				)}
			</div>
			<WebMapView
				setMarker={(location) => {
					setMarker(location);
					// setSelected('');
				}}
				initialMarker={initialMarker}
				marker={marker}
				editable
				zoomTo={{ center, zoom }}
			/>

			<div style={{ bottom: '10px', position: 'absolute', zIndex: 10, width: '100%', display: 'flex' }}>
				<IonButton
					onClick={() => {
						setMarker(undefined);
						close();
					}}
					color="medium"
					style={{
						margin: 'auto',
						color: '#424242',
						height: '50px',
						border: '1px solid #c2c2c2',
						borderRadius: '5px',
					}}
				>
					Back
				</IonButton>
				<IonButton
					color="primary"
					// style={{ background: 'white' }}
					disabled={!marker}
					onClick={() => {
						setLocation(marker, selected || `${marker.lat.toFixed(5)}, ${marker.lng.toFixed(5)}`);
						close();
					}}
				>
					Save
				</IonButton>
			</div>
		</React.Fragment>
	);
};
