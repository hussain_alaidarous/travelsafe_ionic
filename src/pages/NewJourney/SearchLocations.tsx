import React, { useState, useEffect } from 'react';
import { IonButton, IonItem, IonSearchbar, IonList, IonModal } from '@ionic/react';
import Axios from 'axios';
import { WLS_CONFIG, WLS_PROXY } from '../../services/Constants';

interface Props {
	setLocation: (marker, label) => void;
	close: () => void;
}
interface SearchResult {
	layerName: string;
	value: string;
	geometry: {
		x: number;
		y: number;
	};
}

export const SearchLocations = ({ setLocation, close }: Props) => {
	const [ search, setSearch ] = useState('');
	const [ results, setResults ] = useState<SearchResult[]>([]);
	// const [ selected, setSelected ] = useState('');
	const [ searchConfig, setSearchConfig ] = useState<any>();
	const [ geometryConfig, setGeometryConfig ] = useState<any>();
	const getResults = async (searchText) => {
		const res = await Axios.get(
			`${WLS_PROXY}?${searchConfig.url}/find?searchText=${searchText}&layers=${searchConfig.layers}&f=json`,
		);
		const obj = {};
		const results: SearchResult[] = res.data.results.map((result) => ({
			layerName: result.layerName,
			value: result.value,
			geometry: result.geometry,
		}));
		results.forEach((result) => {
			obj[result.value.toUpperCase()] = result;
		});
		const data = Object.keys(obj)
			.map((key) => obj[key])
			.filter((rec, index) => index < 10 && !rec.value.startsWith('http'));
		setResults(data);
	};
	useEffect(
		() => {
			Axios.get(WLS_CONFIG).then((configRes) => {
				setSearchConfig(configRes.data.data.journeyConfig.SearchLayersConfig);
				setGeometryConfig(configRes.data.data.journeyConfig.GeometryServiceConfig);
			});
		}, // eslint-disable-next-line
		[],
	);
	return (
		<React.Fragment>
			<IonModal isOpen={true}>
				<IonSearchbar
					style={{ marginTop: '50px', marginBottom: '20px' }}
					mode="md"
					value={search}
					debounce={700}
					placeholder={'Search For Locations'}
					onIonChange={(event) => {
						setSearch(event.detail.value!);
						if (event.detail.value && event.detail.value.length > 2) {
							getResults(event.detail.value);
						}
					}}
				/>
				<IonList style={{ minHeight: '100%' }}>
					{!results.length ? (
						<IonItem>
							<IonButton style={{ marginTop: '15px' }} color="medium" onClick={() => close()}>
								Back
							</IonButton>
						</IonItem>
					) : (
						results.map((result) => (
							<IonItem
								onClick={async () => {
									const data = {
										geometryType: 'esriGeometryPoint',
										geometries: [ { x: result.geometry.x, y: result.geometry.y } ],
									};
									const res = await Axios.get(
										`${WLS_PROXY}?${geometryConfig.url}/project?inSR=${geometryConfig.inSR}&outSR=${geometryConfig.outSR}&geometries=${encodeURI(
											JSON.stringify(data),
										)}&f=json`,
									);
									const { x, y } = res.data.geometries[0];
									setLocation(
										{
											lat: y,
											lng: x,
										},
										result.value,
									);
									close();
								}}
								key={result.value}
							>
								{result.layerName}: {result.value}
							</IonItem>
						))
					)}
				</IonList>
			</IonModal>
		</React.Fragment>
	);
};
