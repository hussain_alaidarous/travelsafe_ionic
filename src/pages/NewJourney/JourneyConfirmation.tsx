import React from 'react';
import { IonIcon, IonPage, IonContent } from '@ionic/react';
import { checkmark } from 'ionicons/icons';
import { useParams, useHistory } from 'react-router';
import { Header } from '../../components/Header';
import { PrimaryButton, ClearButton } from '../../components/Buttons';

export default function JourneyConfirmation() {
	const { id } = useParams();
	const history = useHistory();
	return (
		<IonPage>
			<Header />
			<IonContent>
				<Success />
				<div style={{ display: 'flex' }}>
					<PrimaryButton
						onClick={() => {
							history.push(`/journey/start/info/${id}`);
						}}
						size="large"
						style={{ margin: 'auto', marginTop: '10px' }}
					>
						View Journey
					</PrimaryButton>
				</div>
			</IonContent>
		</IonPage>
	);
}

function Success() {
	const history = useHistory();
	return (
		<React.Fragment>
			<div style={{ display: 'flex', marginTop: '20%' }}>
				<div style={{ margin: 'auto' }}>
					<div>
						<IonIcon
							style={{ height: '10rem', width: '10rem' }}
							size="large"
							color="success"
							icon={checkmark}
						/>

						<h4>Journey Created</h4>
					</div>
				</div>
			</div>
			<div style={{ textAlign: 'center' }}>
				<p>Your journey has been created</p>
				<ClearButton
					style={{
						width: '54%',
					}}
					onClick={() => {
						history.replace('/home/tab1');
					}}
				>
					Go to my Journeys
				</ClearButton>
			</div>
		</React.Fragment>
	);
}
