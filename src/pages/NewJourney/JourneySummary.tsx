import React, { useState, useContext } from 'react';
import {
	IonPage,
	IonContent,
	IonItem,
	IonList,
	IonText,
	useIonViewDidEnter,
	IonIcon,
	IonLoading,
	IonAlert,
} from '@ionic/react';
import { useHistory } from 'react-router';
import { JourneyContext, initialValue, JourneyUser } from '../../services/JourneyContext';

import { WLS_ADD_JOURNEY } from '../../services/Constants';
import { Header } from '../../components/Header';
import { ClearButton, PrimaryButton } from '../../components/Buttons';
import { removeCircle } from 'ionicons/icons';
import Axios from 'axios';

export default function JourneyDetails() {
	const history = useHistory();
	const [ loading, setLoading ] = useState(false);
	const [ , setServerError ] = useState('');
	const { journey, setJourney } = useContext(JourneyContext);
	const [ duration, setDuration ] = useState('');
	const [ passengers, setPassengers ] = useState([] as JourneyUser[]);
	const [ alert, setAlert ] = useState('');
	useIonViewDidEnter(() => {
		const getDuration = (seconds) => {
			// const seconds = (journey.arrivalDate - journey.departureDate) / 1000;
			const days = Math.floor(seconds / (60 * 60 * 24));
			const hours = Math.floor((seconds - days * 60 * 60 * 24) / (60 * 60));
			const mins = Math.floor((seconds - days * 60 * 60 * 24 - hours * 60 * 60) / 60);
			let temp = '';
			if (days) temp += days + ' days ';
			if (hours) temp += hours + ' hours ';
			if (mins) temp += mins + ' mins ';
			return temp;
		};
		const setInitialPassangers = () => {
			setPassengers(journey.passengers);
		};
		const durationCalc = getDuration((journey.arrivalDate - journey.departureDate) / 1000);
		setDuration(durationCalc);
		setInitialPassangers();
	});
	return (
		<IonPage>
			<Header backButton />
			<IonContent>
				<IonList style={{ padding: '20px' }}>
					<h4>Summary</h4>
					<IonItem>
						<IonText>
							Journey Name: <b>{journey.journeyName}</b>
						</IonText>
					</IonItem>
					<IonItem>
						<IonText>
							Origin: <b>{journey.journeyLocations.journeyStart.name}</b>
						</IonText>
					</IonItem>
					<IonItem>
						<IonText>
							Destination: <b>{journey.journeyLocations.journeyEnd.name}</b>
						</IonText>
					</IonItem>
					<IonItem>
						<IonText>Departure: {new Date(journey.departureDate).toLocaleString()}</IonText>
					</IonItem>
					<IonItem>
						<IonText>Arrival: {new Date(journey.arrivalDate).toLocaleString()}</IonText>
					</IonItem>
					<IonItem>
						<IonText>
							Duration: <b>{duration}</b>
						</IonText>
					</IonItem>
					<IonItem>
						<IonText>
							Estimated Distance:{' '}
							<b>{journey.distanceInKm ? `${journey.distanceInKm} km` : 'Not Calculated'}</b>
						</IonText>
					</IonItem>
					<h4>Driver Info</h4>
					<IonItem>Name: {journey.driver.fullName}</IonItem>
					<IonItem>ORG: {journey.driver.organization}</IonItem>
					<IonItem>Phone: {journey.driver.mobilePhone}</IonItem>
					<IonItem>Satellite: {journey.driver.satellitePhone}</IonItem>
					{journey.passengers &&
						passengers.map((passenger, index) => {
							return (
								<React.Fragment key={index}>
									<h4>
										Passenger {index + 1}{' '}
										<IonIcon
											style={{ fontSize: '1.5rem', marginBottom: '-4px' }}
											color="danger"
											icon={removeCircle}
											onClick={() => {
												const temp = passengers.filter((single, pos) => pos !== index);
												setPassengers(temp);
											}}
										/>
									</h4>
									<IonItem>Name: {passenger.fullName}</IonItem>
									<IonItem>ORG: {passenger.department}</IonItem>
									<IonItem>Phone: {passenger.mobilePhone}</IonItem>
								</React.Fragment>
							);
						})}
					<div style={{ display: 'flex' }}>
						<ClearButton
							onClick={() => history.goBack()}
							style={{
								margin: 'auto',
							}}
						>
							Back
						</ClearButton>
						<PrimaryButton
							disabled={loading}
							onClick={async () => {
								setLoading(true);
								try {
									const journeyData = { ...journey, passengers: [ ...passengers ] };
									const res = await Axios.post(WLS_ADD_JOURNEY, journeyData);
									const data = res.data;
									if (data.success) {
										setAlert('Journey created Successfully');
									} else {
										setServerError(data);
									}
								} catch (ex) {
									console.log('ServerError', ex);
									setServerError(ex);
								}
								setLoading(false);
							}}
							style={{
								margin: 'auto',
							}}
						>
							Submit
						</PrimaryButton>
					</div>
				</IonList>
				<IonLoading isOpen={loading} />
				<IonAlert
					message={alert}
					onDidDismiss={() => setAlert('')}
					isOpen={!!alert}
					buttons={[
						{
							text: 'OK',
							handler: () => {
								setJourney(initialValue);
								history.push(`/home/tab1`);
								window.location.reload();
							},
						},
					]}
				/>
			</IonContent>
		</IonPage>
	);
}
