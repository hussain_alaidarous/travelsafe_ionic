import React, { useState } from 'react';
import { IonIcon, IonAlert } from '@ionic/react';
import { power } from 'ionicons/icons';
import { useHistory } from 'react-router';

export const Logout = () => {
	const [ showAlert, setShowAlert ] = useState(false);
	const history = useHistory();
	return (
		// <IonButton>

		<React.Fragment>
			<IonIcon slot="end" icon={power} onClick={() => setShowAlert(true)} />
			<IonAlert
				isOpen={showAlert}
				onDidDismiss={() => setShowAlert(false)}
				cssClass="my-custom-class"
				header={'Logout'}
				message={'Are you sure you want to logout'}
				buttons={[
					{
						text: 'Cancel',
						role: 'cancel',
					},
					{
						text: 'Yes',
						handler: () => {
							localStorage.removeItem('userData');
							history.replace('/home/tab1');
							window.location.reload();
						},
					},
				]}
			/>
		</React.Fragment>
		// </IonButton>
	);
};
