import React from 'react';
import { IonIcon } from '@ionic/react';
import { checkmarkCircle, time, removeCircle, warning, refreshCircle } from 'ionicons/icons';

interface Props {
	status: string;
	isCard: boolean;
}

export const StatusIcon = ({ status, isCard }: Props) => {
	const style = isCard
		? {
				fontSize: '1.5rem',
				position: 'absolute',
				marginLeft: '-10px',
			}
		: {
				margin: '0 10px 0 0',
				// color: '#0aa5e1',
				fontSize: '1.5rem',
			};
	if (status === 'Late') {
		return <IonIcon color="warning" style={style} icon={time} />;
	}
	return (
		<React.Fragment>
			{status === 'Approved' && <IonIcon color="primary" icon={checkmarkCircle} style={style} />}
			{status === 'Completed' && <IonIcon color="dark" style={style} icon={checkmarkCircle} />}
			{status === 'Active' && <IonIcon color="success" style={style} icon={refreshCircle} />}
			{status === 'Terminated' && <IonIcon color="danger" style={style} icon={removeCircle} />}
			{status === 'Pending Approval' && <IonIcon style={style} icon={time} />}
			{status === 'Late' && <IonIcon color="warning" style={style} icon={time} />}
			{status === 'SOS' && <IonIcon color="danger" style={style} icon={warning} />}
			{status === 'Expired' && <IonIcon color="dark" style={style} icon={time} />}
		</React.Fragment>
	);
};
