import React, { useState, useEffect } from 'react';
import {
  IonToast,
  IonLoading,
  IonInput,
  IonButton,
  IonRadio,
  IonRadioGroup,
  IonLabel,
  IonItem,
  IonCheckbox,
  IonSelect,
  IonSelectOption,
} from '@ionic/react';
import Axios from 'axios';
import { WLS_CONFIG } from '../services/Constants';
import { GreyButton, PrimaryButton } from './Buttons';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';

export function TestingPage({ isNotLoggedIn = false, cancel = () => {} }) {
  const [showLoading, setShowLoading] = useState(false);
  const [showToast, setShowToast] = useState('');
  const [URL, setURL] = useState(
    'https://wlsqa.aramco.com.sa:3006/travelSafe/',
  );
  const [data, setData] = useState('');
  const [method, setMethod] = useState<'GET' | 'POST'>('GET');
  const [response, setResponse] = useState('');
  const [withCreds, setWithCreds] = useState(false);
  const [options, setOptions] = useState([] as any[]);

  useEffect(() => {
    Axios.get(WLS_CONFIG)
      .then((configRes) => {
        setOptions(configRes.data.data.journeyConfig.TestConfig.links);
      })
      .catch(() => {});
  }, []);

  const sendRequest = () => {
    setShowLoading(true);
    Axios(URL, {
      method: method,
      data,
      withCredentials: withCreds,
    })
      .then((res) => {
        console.log(res);
        setResponse(JSON.stringify(res.data));
        setShowToast('Success');
      })
      .catch((error) => {
        console.log(error);
        setResponse(JSON.stringify(error));
        setShowToast('Error');
      })
      .finally(() => {
        setShowLoading(false);
      });
  };

  return (
    <div style={{ marginTop: '20px', overflow: 'scroll' }}>
      <IonItem style={{ marginTop: '20px' }}>
        <IonLabel>URL</IonLabel>
        <IonInput
          value={URL}
          onIonChange={(event) => {
            if (event.detail && event.detail.value) {
              setURL(event.detail.value);
            } else {
              setURL('');
            }
          }}
          placeholder="URL"
        />
      </IonItem>
      {method === 'POST' && (
        <IonItem>
          <IonLabel>DATA</IonLabel>
          <IonInput
            onIonChange={(event) => {
              if (event.detail && event.detail.value) {
                setData(event.detail.value);
              } else {
                setData('');
              }
            }}
            placeholder="JSON Data"
          />
        </IonItem>
      )}
      <IonRadioGroup
        value={method}
        onIonChange={(e) => setMethod(e.detail.value)}
      >
        <IonItem>
          <IonLabel>GET</IonLabel>
          <IonRadio slot="start" value="GET" />
        </IonItem>

        <IonItem>
          <IonLabel>POST</IonLabel>
          <IonRadio slot="start" value="POST" />
        </IonItem>
      </IonRadioGroup>
      <IonItem>
        <IonLabel>With Credentials</IonLabel>
        <IonCheckbox
          slot="start"
          checked={withCreds}
          onIonChange={(event) => setWithCreds(event.detail.checked)}
        />
      </IonItem>
      <IonItem>
        <IonLabel>Set Request</IonLabel>
        <IonSelect
          onIonChange={(event) => {
            setURL(options[event.detail.value].url);
            setData(options[event.detail.value].data);
          }}
        >
          {options.map((rec, index) => (
            <IonSelectOption value={index} key={rec.label}>
              {rec.label}
            </IonSelectOption>
          ))}
        </IonSelect>
      </IonItem>

      <div>
        <IonButton style={{ margin: '10px' }} onClick={() => sendRequest()}>
          Send
        </IonButton>
        <GreyButton style={{ margin: '10px' }} onClick={() => setResponse('')}>
          Clear
        </GreyButton>
        {isNotLoggedIn && (
          <IonButton
            style={{ margin: '10px' }}
            color="dark"
            onClick={() => cancel()}
          >
            Back
          </IonButton>
        )}
      </div>

      <div>
        <PrimaryButton
          onClick={async () => {
            const status = await BackgroundGeolocation.checkStatus().catch(
              (err) => err,
            );
            setResponse(JSON.stringify(status).replaceAll(',', ',\n'));
          }}
        >
          Location Status
        </PrimaryButton>
        <PrimaryButton
          onClick={async () => {
            const location = await BackgroundGeolocation.getCurrentLocation().catch(
              (err) => err,
            );
            setResponse(JSON.stringify(location).replaceAll(',', ',\n'));
          }}
        >
          Current Location
        </PrimaryButton>
      </div>

      <div style={{ overflow: 'scroll' }}>{response}</div>
      <IonLoading
        isOpen={showLoading}
        onDidDismiss={() => setShowLoading(false)}
        message={'Please wait...'}
        // duration={5000}
      />
      <IonToast
        isOpen={!!showToast}
        onDidDismiss={() => setShowToast('')}
        duration={1000}
        message={showToast}
      />
    </div>
  );
}
