import React, { CSSProperties } from 'react';
import { IonButton } from '@ionic/react';

interface Props {
	style?: CSSProperties;
	onClick: () => void | Promise<void>;
	children: any;
	size?: 'large' | 'small';
	color?: string;
	routerLink?: string;
	disabled?: boolean;
}

export const PrimaryButton = ({
	style,
	onClick,
	children,
	size = 'small',
	color = 'primary',
	disabled = false,
	routerLink,
}: Props) => {
	return (
		<IonButton
			disabled={disabled}
			routerLink={routerLink}
			color={color}
			onClick={onClick}
			style={{
				color: 'white',
				margin: '5px',
				height: '50px',
				width: size === 'small' ? '100px' : '200px',
				// border: '1px solid #c2c2c2',
				borderRadius: '5px',
				...style,
			}}
		>
			{children}
		</IonButton>
	);
};

export const DangerButton = (props: Props) => {
	return <PrimaryButton color="danger" {...props} />;
};

export const GreyButton = (props: Props) => {
	return <ClearButton {...props} />;
};

export const ClearButton = (props: Props) => {
	return (
		<PrimaryButton
			color="medium"
			{...props}
			style={{
				...props.style,

				color: '#424242',
				border: '1px solid #c2c2c2',
				borderRadius: '10px',
				background: '#808080',
			}}
		/>
	);
};
