import React, { useState } from 'react';
import { IonHeader, IonToolbar, IonImg, IonButtons, IonBackButton, IonModal, IonIcon } from '@ionic/react';
import safeTravel from '../theme/icon.png';
import { Logout } from './Logout';
import { TestingPage } from './TestingPage';
import { settings } from 'ionicons/icons';

interface Props {
	button?: JSX.Element;
	backButton?: boolean;
	logoutButton?: boolean;
	children?: JSX.Element | string;
	testButton?: boolean;
}

export const Header = ({ children, backButton, logoutButton, testButton }: Props) => {
	const [ testing, setTesting ] = useState(false);
	return (
		<IonHeader>
			<IonToolbar style={{ '--background': 'linear-gradient(to right, #d62d29, #fbc001)' }}>
				<IonImg src={safeTravel} style={{ height: '50px' }} />

				<IonButtons slot="start" style={{ color: 'white' }}>
					{backButton && <IonBackButton style={{ color: 'white' }} />}
				</IonButtons>
				<IonButtons slot="end" style={{ color: 'white', position: 'absolute', right: '10px' }}>
					{logoutButton && <Logout />}

					{testButton && <IonIcon slot="end" icon={settings} onClick={() => setTesting(true)} />}
				</IonButtons>
			</IonToolbar>

			<IonModal isOpen={testing}>
				<TestingPage isNotLoggedIn cancel={() => setTesting(false)} />
			</IonModal>
		</IonHeader>
	);
};
