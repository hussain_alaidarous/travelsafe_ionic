import React, { useState } from 'react';
import { JourneyProvider, initialValue, JourneyType, JourneyData, JourneyLocation } from '../services/JourneyContext';

export default function JourneyHandler({ children }) {
	const [ journey, setJourney ] = useState(initialValue);
	const setJourneyType = (type) => {
		let journeyType: JourneyType =
			type === 1
				? {
						id: 1,
						type: 'Non Routine',
					}
				: {
						id: 2,
						type: 'Routine',
					};
		setJourney({
			...journey,
			journeyType,
		});
	};
	const setJourneyLocationsAndDates = (
		origin: JourneyLocation,
		destination: JourneyLocation,
		departureDate: number,
		arrivalDate: number,
		distanceInKm: number,
	) => {
		setJourney({
			...journey,
			journeyLocations: {
				journeyStart: origin,
				journeyEnd: destination,
			},
			arrivalDate,
			departureDate,
			distanceInKm,
		});
	};
	const setJourneyDetails = async (details: JourneyData) => {
		setJourney({
			...journey,
			...details,
		});
	};

	return (
		<div>
			<JourneyProvider
				value={{
					journey,
					setJourney,
					setJourneyLocationsAndDates,
					setJourneyType,
					setJourneyDetails,
				}}
			>
				{children}
			</JourneyProvider>
		</div>
	);
}
