import React, { useEffect } from 'react';
import { IonCard, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCardContent, IonText, IonIcon } from '@ionic/react';
import { time, chevronForward, star } from 'ionicons/icons';
import { StatusIcon } from '../components/StatusIcon';

export const JourneyCard = ({ name, status, start, end, purpose, isFavorite = false }) => {
	useEffect(() => {}, []);
	let now = new Date(start);
	const from = now.toLocaleString('en-US', {
		day: '2-digit',
		month: 'short',
		hour: '2-digit',
		// minute: '2-digit',
	});
	now.setTime(end);
	const to = now.toLocaleString('en-US', {
		day: '2-digit',
		month: 'short',
		hour: '2-digit',
		// minute: '2-digit',
	});
	return (
		<IonCard mode="ios" style={{ margin: '20px', position: 'relative' }}>
			<div style={{ display: 'flex', float: 'left' }} />

			<IonIcon
				icon={chevronForward}
				style={{ position: 'absolute', right: '10px', top: '45%', fontSize: '1.5rem' }}
			/>
			{isFavorite && (
				<IonIcon
					icon={star}
					style={{ color: '#e77204', position: 'absolute', right: '10px', top: '10%', fontSize: '1.5rem' }}
				/>
			)}

			<div style={{ display: 'inline-block' }}>
				<IonCardHeader>
					<div style={{ position: 'relative' }}>
						<StatusIcon isCard status={status} />
						<IonCardSubtitle style={{ marginLeft: '25px' }}>
							<strong style={{ fontSize: '1rem', color: '#666666' }}>{status}</strong>
						</IonCardSubtitle>
						{/* <IonText style={{ color: '#a0a0a0', marginLeft: '25px' }}>{purpose}</IonText> */}
					</div>

					<IonCardTitle style={{ fontSize: '1.4rem', color: '#1b1b1b', marginLeft: '25px' }}>
						{name}
					</IonCardTitle>
				</IonCardHeader>

				<IonCardContent>
					<div style={{ position: 'relative' }}>
						<IonIcon
							style={{ color: '#c2c2c2', fontSize: '1.1rem', position: 'absolute', marginLeft: '25px' }}
							icon={time}
						/>
						<IonText style={{ marginLeft: '50px' }}>
							{from} to {to}
						</IonText>
					</div>
				</IonCardContent>
			</div>
		</IonCard>
	);
};

//wkid 3857
