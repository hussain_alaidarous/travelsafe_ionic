import React, { useRef, useEffect, useState } from 'react';
import { Map, Marker } from 'react-leaflet';
// import * as vector from 'esri-leaflet-vector';
import * as esri from 'esri-leaflet';
import { WLS_CONFIG, WLS_PROXY } from '../services/Constants';
import Axios from 'axios';

interface Props {
	zoomTo: any;
	setMarker?: (location) => void;
	marker?: any;
	editable: boolean;
	initialMarker?: { lat: number; lng: number };
}

export const WebMapView = ({ setMarker, initialMarker, marker, zoomTo }: Props) => {
	const mapRef = useRef<Map>();
	const [ center, setcenter ] = useState<any>();
	const [ zoom, setZoom ] = useState<any>();

	useEffect(
		() => {
			if (zoomTo && (zoomTo.center !== center || zoomTo.zoom !== zoom)) {
				setZoom(zoomTo.zoom);
				setcenter(zoomTo.center);
			}
		},
		[ zoomTo, zoom, center ],
	);
	useEffect(
		() => {
			const getMapConfig = async () => {
				const configRes = await Axios.get(WLS_CONFIG);
				const empConfig = configRes.data.data.journeyConfig;
				// console.log(esri);
				if (mapRef.current) {
					const { DYNAMIC_LIST, TILED_LIST } = empConfig;
					DYNAMIC_LIST.map((MAP_DATA) => {
						// esri.get(`${MAP_PROXY}?${url}`, {}, { headers: 'test' });
						const config = { url: MAP_DATA.url };
						if (MAP_DATA.token) {
							config['token'] = MAP_DATA.token;
						} else if (MAP_DATA.proxy) {
							config['proxy'] = WLS_PROXY;
						}
						esri
							.dynamicMapLayer({
								...config,
								...MAP_DATA.config,
							})
							.addTo(mapRef.current.leafletElement);
						return config;
					});
					TILED_LIST.map((MAP_DATA) => {
						const config = { url: MAP_DATA.url };
						if (MAP_DATA.token) {
							config['token'] = MAP_DATA.token;
						} else if (MAP_DATA.proxy) {
							config['proxy'] = WLS_PROXY;
						}
						esri
							.tiledMapLayer({
								...config,
								...MAP_DATA.config,
							})
							.addTo(mapRef.current.leafletElement);
						return config;
					});
					// VECTOR_LIST.map((MAP_DATA) => {
					// 	console.log(MAP_DATA);
					// 	const config = { url: MAP_DATA.url };
					// 	if (MAP_DATA.token) {
					// 		config['token'] = MAP_DATA.token;
					// 	} else if (MAP_DATA.proxy) {
					// 		config['proxy'] = WLS_PROXY;
					// 	}
					// 	vector
					// 		.vectorTileLayer(config.url, {
					// 			...config,
					// 			...MAP_DATA.config,
					// 		})
					// 		.addTo(mapRef.current.leafletElement);
					// 	return config;
					// });
					if (initialMarker && initialMarker.lat && initialMarker.lng && !marker) {
						if (setMarker) {
							setMarker(initialMarker);
							// setZoom(12);
						}
					}
				}
			};
			getMapConfig();
		}, // eslint-disable-next-line
		[ mapRef, initialMarker, setMarker ],
	);
	return (
		<div style={{ height: '800px' }}>
			<Map
				style={{ height: '100vh', width: '100%', margin: '0', zIndex: 1 }}
				center={center}
				zoom={zoom}
				ref={mapRef}
				onClick={(info) => {
					if (setMarker) {
						setMarker(info.latlng);
					}
				}}
			>
				{marker && <Marker position={marker} />}
			</Map>
		</div>
	);
};

//wkid 3857
