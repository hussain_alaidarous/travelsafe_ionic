import Axios from 'axios';
import React from 'react';

import { ADUser } from './UserContext';
import { WLS_ADD_JOURNEY } from './Constants';
import { JourneyStatus } from './JourneyStatus';

interface Routine {
  id: 2;
  type: 'Routine';
}
interface NonRoutine {
  id: 1;
  type: 'Non Routine';
}

export type JourneyType = Routine | NonRoutine;
export interface JourneyInput {
  journeyName: string;
  journeyLocations: {
    journeyStart: JourneyLocation;
    journeyEnd: JourneyLocation;
  };
  journeyType: JourneyType;
  departureDate: number;
  arrivalDate: number;
  distanceInKm: number;
  purpose: string;
  requester: ADUser;
  driver: JourneyUser;
  passengers: JourneyUser[];
}

export type Journey = JourneyInput & {
  journeyStatus: JourneyStatus;
  journeyId: number;
};

export interface JourneyLocation {
  name: string;
  geometry: {
    x: number;
    y: number;
    spatialReference?: {
      wkid: number;
    };
  };
}

export type JourneyUser = ADUser & {
  mobilePhone: string;
  satellitePhone?: '' | string;
};

export interface JourneyData {
  journeyName: string;
  purpose: string;
  // distanceInKm: number;
  requester: ADUser;
  driver: JourneyUser;
  passengers: JourneyUser[];
}

interface JourneyContextType {
  journey: Journey | JourneyInput;
  setJourney(journey: Journey | JourneyInput);
  setJourneyType(type: number);
  setJourneyLocationsAndDates(
    origin: JourneyLocation,
    destination: JourneyLocation,
    departureDate: number,
    arrivalDate: number,
    distanceInKm: number
  );
  setJourneyDetails(details: JourneyData);
}

export const JourneyContext = React.createContext<JourneyContextType>({
  setJourney: () => { },
  setJourneyType: () => { },
  setJourneyLocationsAndDates: () => { },
  setJourneyDetails: async () => { },
  journey: {} as JourneyInput,
});

export const JourneyProvider = JourneyContext.Provider;
export const JourneyConsumer = JourneyContext.Consumer;

export const addJourney = async (journey) => {
  //   const options = {
  //     withCredentials: true,
  //     method: 'POST',
  //     data: `journeyJson=${JSON.stringify(journey)}`,
  //   };
  try {
    const res = await Axios.post(WLS_ADD_JOURNEY, journey);
    return res.data;
  } catch (error) {
    return error;
  }
};
// GPS location wkid 3857
export const initialValue: JourneyInput = {
  journeyName: '',
  journeyLocations: {
    journeyStart: {
      name: '',
      geometry: {
        x: 0,
        y: 0,
        spatialReference: {
          wkid: 4326,
        },
      },
    },
    journeyEnd: {
      name: '',
      geometry: {
        x: 0,
        y: 0,
        spatialReference: {
          wkid: 4326,
        },
      },
    },
  },
  journeyType: {
    id: 2,
    type: 'Routine',
  },
  departureDate: 0,
  arrivalDate: 0,
  distanceInKm: 0,
  purpose: '',
  requester: {
    networkId: '',
    fullName: '',
    organization: '',
    department: '',
  },
  driver: {
    networkId: '',
    fullName: '',
    organization: '',
    department: '',
    mobilePhone: '',
    satellitePhone: '',
  },
  passengers: [],
};
