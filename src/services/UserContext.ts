import React from 'react';

export interface ADUser {
    networkId: string
    fullName: string
    department: string
    organization: string
}

export const UserContext = React.createContext<ADUser | undefined>(undefined);
export const UserProvider = UserContext.Provider;
export const UserConsumer = UserContext.Consumer;