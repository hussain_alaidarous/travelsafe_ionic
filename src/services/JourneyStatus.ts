export interface Approved { "id": 1, "status": "Approved" }
export interface Pending { "id": 2, "status": "Pending Approval" }
export interface Active { "id": 3, "status": "Active" }
export interface Completed { "id": 4, "status": "Completed" }
export interface Canceled { "id": 5, "status": "Canceled" }
export interface Terminated { "id": 6, "status": "Terminated" }
export interface Late { "id": 7, "status": "Late" }
export interface Rejected { "id": 8, "status": "Rejected" }
export interface SOS { "id": 9, "status": "SOS" }
export interface Expired { "id": 10, "status": "Expired" }
export type JourneyStatus = Approved | Pending | Active | Completed | Canceled | Terminated | Late | Rejected | SOS | Expired;
