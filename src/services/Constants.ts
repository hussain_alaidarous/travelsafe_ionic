export const APP_NAME = 'SafeTravel';
// export const WLS = 'http://3.7.60.135:9000';
const isProd = false;
export const WLS = isProd ? 'https://maps.aramco.com.sa/travelSafe' : 'https://wlsqa.aramco.com.sa:3006/travelSafe';
export const WLS_PROXY = `${WLS}/rest/mobileProxy`
export const WLS_USER_AUTH = `${WLS}/auth`;
export const WLS_USER_INFO = `${WLS}/rest/user-info`;
export const WLS_USER_LAST_LOGIN = `${WLS}/rest/user-last-login`;
export const WLS_WEBSTAT = `${WLS}/rest/user-webstat`;
export const WLS_USER_POSITION = `${WLS}/rest/user-positions`;
export const WLS_USER_JOURNEYS = `${WLS}/rest/user-journeys`;
export const WLS_USER_JOURNEY = `${WLS}/rest/user-journey`;
export const WLS_JOURNEY_CHECK = `${WLS}/rest/journey-status`;
export const WLS_START_JOURNEY = `${WLS}/rest/start-user-journey`;
export const WLS_COMPLETE_JOURNEY = `${WLS}/rest/complete-user-journey`;
export const WLS_CANCEL_JOURNEY = `${WLS}/rest/cancel-user-journey`;
export const WLS_UPDATE_JOURNEY = `${WLS}/rest/update-user-journey`;
export const WLS_ADD_JOURNEY = `${WLS}/rest/user-journey`;
export const WLS_SOS_JOURNEY = `${WLS}/rest/user-sos-journey`;
export const WLS_CONFIG = `${WLS}/rest/journey-config`;
// export const MAP_TOKEN =
//   'GqPt2iUa7El6N55LlcX7_B1QyXURTXTzONfrfuxCO8hqfa_Wlly1B_dB0yFwzwNq65dU-664TLx11hGNKxz2OZhR_Gn9NxMykQWgqlQgN0w.';
// export const MAP_JOURNEY_URL =
//   'https://arcgismapsqa.aramco.com.sa:6443/arcgis/rest/services/TravelSafe/Journeys/MapServer/0';
// export const MAP_EXTRA_URL =
//   'https://arcgismapsqa.aramco.com.sa:6443/arcgis/rest/services/TravelSafe/TravelSafe/MapServer';
// export const MAP_URL_2 =
//   'https://arcgismapsqa.aramco.com.sa:6443/arcgis/rest/services/EMAP_Services/Labels_WGS84/MapServer';
// export const MAP_URL =
//   'https://arcgismapsqa.aramco.com.sa:6443/arcgis/rest/services/EMAP_Services/Imagery_WebMercator/MapServer';
