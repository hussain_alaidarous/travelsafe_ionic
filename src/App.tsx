import React, { useState, useEffect } from 'react';
import { Redirect, Route } from 'react-router-dom';
import {
  IonApp,
  IonRouterOutlet,
  IonModal,
  IonToast,
  IonLoading,
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
// import "@ionic/react/css/structure.css";
import '@ionic/react/css/typography.css';

import './theme/variables.css';
/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import TabsRouter from './pages/Home/TabsRouter';
import JourneyDetails from './pages/NewJourney/JourneyDetails';
import JourneyNew from './pages/NewJourney/JourneyNew';
import JourneyConfirmation from './pages/NewJourney/JourneyConfirmation';
import JourneyCheck from './pages/NewJourney/JourneyCheck';
import JourneySummary from './pages/NewJourney/JourneySummary';

import { UserProvider, ADUser } from './services/UserContext';
import JourneyHandler from './components/JourneyHandler';
import {
  WLS_USER_INFO,
  WLS_WEBSTAT,
  WLS_USER_LAST_LOGIN,
} from './services/Constants';
import JourneyInfo from './pages/StartJourney/JourneyInfo';
import JourneyChecklist from './pages/StartJourney/JourneyChecklist';
import Login from './pages/Login';

import { Plugins } from '@capacitor/core';

import Axios from 'axios';
Axios.defaults.withCredentials = false;

const { Device } = Plugins;

const App: React.FC = () => {
  const [user, setUser] = useState<undefined | ADUser>({} as ADUser);
  const [active, setActive] = useState<any>();
  const [lastLogin, setLastLogin] = useState('');

  useEffect(() => {
    const getUser = async () => {
      // localStorage.removeItem('map_cache');
      try {
        // localStorage.removeItem('userData');
        const isActive = localStorage.getItem('active_journey');
        setActive(isActive);
        const userData = localStorage.getItem('userData');

        if (!userData) {
          setUser(undefined);
          Axios.defaults.auth = {
            username: '',
            password: '',
          };
          return;
        }
        const jsonData = JSON.parse(userData);
        const res = await Axios.get(
          `${WLS_USER_INFO}?networkId=${jsonData.networkId}`,
          {
            auth: {
              username: jsonData.networkId,
              password: jsonData.password,
            },
          },
        );
        Axios.defaults.auth = {
          username: jsonData.networkId,
          password: jsonData.password,
        };
        if (window.location.pathname !== '/journey/new') {
          try {
            const lastLoginInfo = (await Axios.get(WLS_USER_LAST_LOGIN)).data;
            if (lastLoginInfo.data) {
              const date = new Date(lastLoginInfo.data);
              setLastLogin(`Last Login on: ${date.toLocaleString()}`);
            }

            const info = await Device.getInfo();
            await Axios.post(WLS_WEBSTAT, {
              widgetName: 'SafeTravel iOS',
              event: 'Application Loaded 1.3.42',
              userAgent: `iPod/iPhone, Mobile Safari 1.3.42, iOS ${info.osVersion}`,
            });
          } catch (ex) {}
        }

        const data = res.data;
        if (data.success) {
          setUser(data.data);
        } else {
          localStorage.removeItem('userData');
          setUser(undefined);
          Axios.defaults.auth = {
            username: '',
            password: '',
          };
        }
      } catch (error) {
        localStorage.removeItem('userData');
        console.error(error);
        setUser(undefined);
      }
    };
    // getUser();
    setUser(undefined);
  }, []);
  if (!user) {
    return (
      <IonApp>
        <IonModal isOpen>
          <Login />
        </IonModal>
      </IonApp>
    );
  } else if (!user.networkId) {
    return <IonLoading isOpen />;
  }
  return (
    <IonApp>
      <UserProvider value={user}>
        <JourneyHandler>
          <IonReactRouter>
            <IonRouterOutlet>
              <Route path="/journey/new" component={JourneyNew} exact />
              {/* <Route path="/journey/new/location" component={JourneyLocations} exact /> */}
              <Route path="/journey/new/type" component={JourneyCheck} exact />
              <Route
                path="/journey/new/details"
                component={JourneyDetails}
                exact
              />
              <Route
                path="/journey/new/summary"
                component={JourneySummary}
                exact
              />
              <Route
                path="/journey/new/confirmation/:id"
                component={JourneyConfirmation}
                exact
              />
              <Route
                path="/journey/start/info/:id"
                component={JourneyInfo}
                exact
              />
              <Route
                path="/journey/start/checklist/:id"
                component={JourneyChecklist}
                exact
              />
              <Route path="/home" component={TabsRouter} />
              <Route
                path="/"
                render={() =>
                  active ? (
                    <Redirect to="/home/tab2" />
                  ) : (
                    <Redirect to="/home" />
                  )
                }
                exact={true}
              />
            </IonRouterOutlet>
          </IonReactRouter>
        </JourneyHandler>
        <IonToast
          isOpen={!!lastLogin}
          message={lastLogin}
          duration={3000}
          onDidDismiss={() => {
            setLastLogin('');
          }}
        />
      </UserProvider>
    </IonApp>
  );
};

export default App;
