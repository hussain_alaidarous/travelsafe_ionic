#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DummyClass : NSObject
+ (void)dummyCall;
@end

NS_ASSUME_NONNULL_END